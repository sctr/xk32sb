/*
 * if_timer.h
 *
 * Created: 16.09.2021 14:13:08
 *  Author: vB1w
 */ 


#ifndef IF_TIMER_H_
#define IF_TIMER_H_

#include "xk32/xk32_timer.h"

typedef struct if_timer {
	
	}if_timer;

if_timer s_if_timer;
xk32_timer s_xk32_timer;

void if_timer_init(if_timer *pself);




#endif /* IF_TIMER_H_ */