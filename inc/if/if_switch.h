/*
 * if_switch.h
 *
 * Created: 13.10.2018 20:48:59
 *  Author: vB1w
 *	Description: interface for application to manage switches
 */ 


#ifndef IF_SWITCH_H_
#define IF_SWITCH_H_

#include "drv/atsame70q20/afec.h"
#include "xk32/xk32_term.h"
#include "app/sb/mot/sb_mot_ctrl.h"

#define PUSHED		0x01U
#define RELEASED	0x00U

typedef void (*fct)(void);

typedef struct if_switch{
	const char *name;
	const char *description;
	uint8_t is_init;
	uint8_t is_trig;
	uint8_t port;
	uint8_t pin_num;
	fct *callback;
	}if_switch;

	
void if_switch_init(struct if_switch *pself);
void if_switch_run(struct if_switch *pself, uint8_t is_on);
void if_switch_on_switch1(void);
void if_switch_on_switch2(void);
void if_switch_on_switch3(void);
void if_switch_on_switch4(void);


#endif /* IF_SWITCH_H_ */
