/*
 * if_term.h
 *
 * Created: 10.02.2021 18:34:26
 *  Author: vB1w
 *	Description: interface for application to get and set terminal commands
 */ 


#ifndef IF_TERM_H_
#define IF_TERM_H_

#include "xk32/xk32_term.h"

typedef struct if_term {
	uint8_t is_init;
	uint8_t enable;
	
	uint32_t baudrate;
	uint8_t parity;
	}if_term;
	
if_term s_sb_term;
	
void if_term_init(struct if_term *pself);
void if_term_run(struct if_term *pself);


#endif /* IF_TERM_H_ */