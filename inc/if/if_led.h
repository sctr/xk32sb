/*
 * if_led.h
 *
 * Created: 27.07.2019 01:47:16
 *  Author: vB1w
 *	Description: interface for led control of an application
 */ 


#ifndef IF_LED_H_
#define IF_LED_H_

#include "xk32/xk32_gpio.h"
#include "xk32/xk32_timer.h"

typedef struct if_led_dedicated_led {
	uint8_t toggling;
	uint16_t toggle_time_on;
	uint16_t toggle_time_off;
	uint32_t ctr_update;
	uint32_t ctr_irq;
	uint8_t change;
	uint8_t state;
	char *name;
	uint8_t port;
	uint8_t pin;
}if_led_dedicated_led;

struct if_led_dedicated_led s_if_led0;
struct if_led_dedicated_led s_led1;
struct if_led_dedicated_led s_if_led2;
struct if_led_dedicated_led s_led3;

void if_led_init(void);
void if_led_register(if_led_dedicated_led *pself);
void if_led_update(void);
void if_led_run(struct if_led_dedicated_led *pself);
void if_led_set(struct if_led_dedicated_led *pself, uint8_t new_state);
void if_led_on_irq(void *pself);

void if_led0_on(void);
void if_led0_off(void);

#endif /* IF_LED_H_ */
