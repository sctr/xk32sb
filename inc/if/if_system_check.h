/*
 * if_system_check.h
 *
 * Created: 25.01.2021 13:52:01
 *  Author: vB1w
 *	Description: interface for application to check the system
 */ 


#ifndef IF_SYSTEM_CHECK_H_
#define IF_SYSTEM_CHECK_H_

/* TODO:: consider architecture*/
#include "drv/atsame70q20/wdt.h"

typedef struct if_system_check  {
	uint8_t is_active;
}if_system_check;

struct if_system_check s_sb_syschk;

void if_sys_check_init(struct if_system_check *pself);

void if_sys_check_run(struct if_system_check *pself);


#endif /* IF_SYSTEM_CHECK_H_ */