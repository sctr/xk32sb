/*
 * sb_pm.h
 *
 * Created: 22.01.2021 09:45:43
 *  Author: vB1w
 */ 


#ifndef SB_PM_H_
#define SB_PM_H_

#include "xk32/xk32_clock.h"

typedef struct if_pm{
	uint8_t is_initialized;
	uint32_t clock_speed_desired;
	uint32_t clock_speed_measured;
//	pmc pwr_mng_ctrl;
// 	system_clock sys_clk;
// 	peripheral_clock periph_clk;
	
}if_pm;
	
struct if_pm s_if_pm;
struct xk32_clock s_clock;
	
void if_pm_init(struct if_pm *pself);
void if_pm_run(struct if_pm *pself);
void if_pm_check(struct if_pm *pself);


#endif /* SB_PM_H_ */