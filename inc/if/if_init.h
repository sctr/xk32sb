/*
 * if_init.h
 *
 * Created: 21.01.2021 12:21:56
 *  Author: vB1w
 *	Description: interface for initialization of an application
 */ 


#ifndef IF_INIT_H_
#define IF_INIT_H_

#include "if/if_pm.h"

/* register tasks*/
int if_init(void);
/* 1ms init task, something changed?*/
int if_init_update(void);
/* init power management*/
int if_init_pm(void);

#endif /* IF_INIT_H_ */