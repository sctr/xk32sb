/*
 * com.h
 *
 * Created: 06.09.2021 11:41:22
 *  Author: vB1w
 */ 


#ifndef COM_H_
#define COM_H_

#include "xk32/xk32_term_msg_list.h"

#define CMD_SEND	0
#define CMD_RECV	1

typedef void (*fct)(void);

typedef struct com_socket_list{
	uint8_t *list;
	
}com_socket_list;

typedef struct com_cmd{
	uint8_t send;
	fct *cmd;
	}com_cmd;
	
typedef struct com_cmd_list{
	void *list;
}com_cmd_list;
	
typedef struct com{
	com_socket_list *socket_list;
	com_cmd_list *cmd_list;
}com;


void if_com_init(struct com *pself);
void if_com_run(struct com *pself);
void if_com_get_socket_list(struct com *pself);
void com_cmd_send(struct com *pself);


#endif /* COM_H_ */