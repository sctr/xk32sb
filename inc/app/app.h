/*
 * app.h
 *
 * Created: 19.01.2021 09:40:33
 *  Author: vB1w
 */ 


#ifndef APP_H_
#define APP_H_

#include "sam.h"

#include "drv/atsame70q20/defs.h"
#include "drv/atsame70q20/pio.h"
#include "drv/atsame70q20/nvic.h"
#include "drv/atsame70q20/pmc.h"
#include "drv/atsame70q20/afec.h"

#include "xk32/xk32_task.h"
#include "xk32/xk32_clock.h"
#include "xk32/xk32_timer.h"
#include "xk32/xk32_term.h"

#include "if/if_pm.h"
#include "if/if_timer.h"
#include "if/if_system_check.h"
#include "if/if_term.h"
#include "if/if_led.h"

#include "app/sb/mot/sb_mot_ctrl.h"


typedef struct app{
	char *name;
	char *info;
	uint8_t version;
}app;

struct app	s_app_sb;
struct app	s_ban_app;

void app_init(struct app *pself);
void app_run(void);



#endif /* APP_H_ */