/*
 * ban.h
 *
 * Created: 09.09.2021 07:03:17
 *  Author: vB1w
 */ 


#ifndef BAN_H_
#define BAN_H_

#include "sam.h"

#include "app/ban/bms/ban_def.h"
#include "app/ban/bms/ban_init.h"
#include "app/ban/bms/ban_package.h"
#include "app/ban/bms/ban_cell.h"

typedef struct ban{
	uint8_t		b_is_init;
	ban_package	pack;
	uint8_t		pack_total;
	uint8_t		u8_err;
}ban;

struct ban s_ban;

void ban_init(void);
void ban_run(void);


#endif /* BAN_H_ */