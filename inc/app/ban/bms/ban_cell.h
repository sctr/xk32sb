/*
 * ban_cell.h
 *
 * Created: 09.09.2021 07:29:11
 *  Author: vB1w
 */ 


#ifndef BAN_CELL_H_
#define BAN_CELL_H_

#include "app/ban/ban.h"

typedef struct ban_cell{
	uint8_t		cell_num;
	uint8_t		voltage;
	uint8_t		soh;		//state of health
	uint8_t		err;
}ban_cell;

ban_cell s_ban_cell;



#endif /* BAN_CELL_H_ */