/*
 * ban_bms_ctrl.h
 *
 * Created: 07.09.2021 13:49:24
 *  Author: vB1w
 *	Info: Abbreviation "ban" stands for "banana":
 *	It labels the Battery Management System BMS project
 */ 


#ifndef BMS_CTRL_H_
#define BMS_CTRL_H_

#include "app/ban/bms/ban_calc.h"


void bms_ctrl_init(struct ban *pself);

void bms_ctrl_run(struct ban *pself);


#endif /* BMS_CTRL_H_ */