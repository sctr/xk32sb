/*
 * ban_package.h
 *
 * Created: 09.09.2021 07:28:53
 *  Author: vB1w
 */ 


#ifndef BAN_PACKAGE_H_
#define BAN_PACKAGE_H_

#include "app/ban/bms/ban_cell.h"

typedef struct ban_package{
	uint8_t		package_num;
	struct ban_cell	*ps_cell;
	uint8_t		err;
}ban_package;


#endif /* BAN_PACKAGE_H_ */