/*
 * ban_calc_cell.h
 *
 * Created: 09.09.2021 07:15:36
 *  Author: vB1w
 */ 


#ifndef BAN_CALC_CELL_H_
#define BAN_CALC_CELL_H_

#include "app/ban/bms/ban_cell.h"

void ban_cell_calc(struct ban_cell *pself);
void ban_cell_calc_soh(struct ban_cell *pself);
void ban_cell_calc_energy(struct ban_cell *pself);
void ban_cell_calc_err(struct ban_cell *pself);

#endif /* BAN_CALC_CELL_H_ */