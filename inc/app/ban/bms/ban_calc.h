/*
 * ban_calc.h
 *
 * Created: 09.09.2021 07:15:59
 *  Author: vB1w
 */ 


#ifndef BAN_CALC_H_
#define BAN_CALC_H_

#include "app/ban/ban.h"

void ban_calc(struct ban *pself);
void ban_calc_err(struct ban *pself);


#endif /* BAN_CALC_H_ */