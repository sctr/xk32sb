/*
 * ban_calc.h
 *
 * Created: 09.09.2021 06:46:36
 *  Author: vB1w
 */ 


#ifndef BAN_BMS_CALC_H_
#define BAN_BMS_CALC_H_

#include "bms/ban_package.h"

void ban_calc_package(struct ban_package *pself);
void ban_calc_package_soh(struct ban_package *pself);
void ban_calc_package_energy(struct ban_package *pself);
void ban_calc_package_err(struct ban_package *pself);


#endif /* BAN_BMS_CALC_H_ */