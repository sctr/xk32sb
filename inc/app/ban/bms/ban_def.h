/*
 * ban_def.h
 *
 * Created: 09.09.2021 07:27:49
 *  Author: vB1w
 */ 


#ifndef BAN_DEF_H_
#define BAN_DEF_H_

#define PACKAGE_TOTAL		1
#define CELL_TOTAL			12


#define PACKAGE_AMP_MAX		20
#define CELL_AMP_MAX		5



#endif /* BAN_DEF_H_ */