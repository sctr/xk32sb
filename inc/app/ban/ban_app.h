/*
 * ban_app.h
 *
 * Created: 09.09.2021 08:11:15
 *  Author: vB1w
 */ 


#ifndef BAN_APP_H_
#define BAN_APP_H_


#include "sam.h"

#include "drv/atsamd10c/defs.h"
#include "drv/atsamd10c/pio.h"
#include "drv/atsamd10c/nvic.h"
#include "drv/atsamd10c/pmc.h"
#include "drv/atsamd10c/afec.h"

#include "xk32/xk32_task.h"
#include "xk32/xk32_clock.h"
#include "xk32/xk32_timer.h"
#include "xk32/xk32_term.h"

#include "app/ban/ban.h"

typedef struct ban_app{
	char *name;
	char *info;
	uint8_t version;
}ban_app;

struct ban_app s_ban_app;

void ban_app_init(struct ban_app *pself);
void ban_app_run(void);

#endif /* BAN_APP_H_ */