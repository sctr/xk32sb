/*
 * sb_mot_ctrl.h
 *
 * Created: 16.11.2018 19:36:51
 *  Author: vB1w
 *	Info: Abbreviation "sb" stands for "strawberry":
 *	It labels the Motor Control project
 */ 


#ifndef SB_MOT_CTRL_H_
#define SB_MOT_CTRL_H_

#include "drv/atsame70q20/defs.h"
#include "drv/atsame70q20/pio.h"
#include "drv/atsame70q20/pwm.h"
#include "app/sb/mot/commutation/sb_comu_step_man_table.h"
#include "app/sb/mot/commutation/sb_comu_block.h"
#include "app/sb/mot/commutation/sb_comu_step_man.h"

enum sb_mot_ctrl_comu
{
	comu_step_manual,
	comu_step,
	comu_block,
	comu_foc
	};
	
typedef struct sb_mot_ctrl{
	enum sb_mot_ctrl_comu e_sb_mot_ctrl_comu;
	uint8_t is_init;
	uint8_t init_ctr_max;
	uint8_t init_ctr;
	uint8_t cmd_load_cap; // for testing, will later be implemented before .cmd_start
	uint8_t cmd_start;
	}sb_mot_ctrl;

struct sb_mot_ctrl s_sb_mot_ctrl;

void sb_mot_ctrl_init(struct sb_mot_ctrl *pself);

void sb_mot_ctrl_update(struct sb_mot_ctrl *pself);

void sb_mot_ctrl_cmd_load_cap(void);

void sb_mot_ctrl_cmd_start(void);

void sb_mot_ctrl_cmd_stop(void);

#endif /* SB_MOT_CTRL_H_ */
