/*
 * sb_comu_step_man_table.h
 *
 * Created: 22.10.2018 17:07:15
 *  Author: vB1w
 */ 


#ifndef SB_COMU_STEP_MAN_TABLE_H_
#define SB_COMU_STEP_MAN_TABLE_H_

#include "sam.h"

#define SB_MOT_PHASE	0x03u
#define PHASE_HIGH		0x01u
#define PHASE_LOW		0x00u

typedef struct sb_comu_step_man_table{
	uint8_t phaseUH;
	uint8_t phaseUL;
	uint8_t phaseVH;
	uint8_t phaseVL;
	uint8_t phaseWH;
	uint8_t phaseWL;
	}sb_comu_step_man_table;

struct sb_comu_step_man_table s_sb_comu_step_man_table[SB_MOT_PHASE];

#endif /* SB_MOT_CTRL_TABLE_H_ */