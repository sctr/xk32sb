/*
 * sb_comu_block.h
 *
 * Created: 24.07.2019 22:15:59
 *  Author: vB1w
 */ 


#ifndef SB_COMU_BLOCK_H_
#define SB_COMU_BLOCK_H_

#include "sam.h"

enum phase
{
	ph_a,
	ph_b,
	ph_c
};

typedef struct sb_comu_block {
	enum phase e_phase;
	uint32_t speed_is;
	uint32_t speed_should;
	uint32_t torque_is;
	uint32_t torque_should;
	uint32_t current_is;
	uint32_t current_should;
	uint32_t angle_electric;
	uint32_t angle_absolute;
	}sb_comu_block;

void sb_comu_block_init(struct sb_comu_block *pself);
void sb_comu_block_run(struct sb_comu_block *pself);

#endif /* SB_COMU_BLOCK_H_ */