/*
 * sb_comu_step_man.h
 *
 * Created: 24.07.2019 22:28:02
 *  Author: vB1w
 */ 


#ifndef SB_COMU_STEP_MAN_H_
#define SB_COMU_STEP_MAN_H_

#include "drv/atsame70q20/defs.h"
#include "drv/atsame70q20/pio.h"
#include "drv/atsame70q20/pwm.h"
#include "xk32/xk32_timer.h"
#include "app/sb/mot/commutation/sb_comu_step_man_table.h"

typedef struct sb_comu_step_man{
	const uint16_t time_load_cap;	//ms
	const uint16_t time_commutation;	//ms
	struct sb_comu_step_man_table *s_sb_comu_step_man_table;
	uint8_t table_ctrl_ctr; /*only from zero to two!*/
	}sb_comu_step_man;
	
struct sb_comu_step_man s_sb_comu_step_man;

void sb_comu_step_man_init(struct sb_comu_step_man *pself);
void sb_comu_step_man_update(struct sb_comu_step_man *pself);
void sb_comu_step_man_start(void);
void sb_comu_step_man_load_cap(void);

void sb_comu_step_man_trigger_all_low_side(void);
void sb_comu_step_man_trigger_phase(void);

void sb_comu_step_man_next_phase(struct sb_comu_step_man *s_step_man);

void sb_comu_step_man_turn_off(void);

void sb_comu_step_man_low_side_all(struct sb_comu_step_man *pself);

void sb_comu_step_man_trigger_rtt(void);


#endif /* SB_COMU_STEP_MAN_H_ */
