/*
 * gpio.h
 *
 * Created: 27.07.2019 21:42:26
 *  Author: vB1w
 */ 


#ifndef GPIO_H_
#define GPIO_H_

#include "drv/atsame70q20/pio.h"

#define PORTA 0
#define PORTB 1
#define PORTC 2
#define PORTD 3

void xk32_gpio_register_led(uint8_t port, uint8_t pin_num, char *purpose);
void xk32_gpio_register_switch(uint8_t port, uint8_t pin_num, char *purpose);
void xk32_gpio_register_pio(uint8_t port, uint8_t pin_num, char *purpose);
void xk32_gpio_set(uint8_t port, uint8_t pin_num, uint8_t led_switch, char *purpose);
void xk32_gpio_overwrite(
		uint8_t port,
		uint8_t pin_num,
		char *pfct,
		uint8_t pio_endis,
		uint8_t io,
		uint8_t pupd,
		uint8_t multidriver,
		uint8_t owrite,
		uint8_t interrupt,
		uint8_t default_val,
		uint8_t periph,
		uint8_t i_edge_sel,
		uint8_t i_rehl_sel,
		uint8_t i_add_irq_mode
		);



#endif /* GPIO_H_ */
