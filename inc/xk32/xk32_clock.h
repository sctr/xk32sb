/*
 * xk32_clock.h
 *
 * Created: 06.08.2019 23:49:30
 *  Author: vB1w
 */ 


#ifndef XK32_CLOCK_H_
#define XK32_CLOCK_H_

#include "drv/atsame70q20/pmc.h"

typedef enum {
	SLOW_CLK = 0,
	MAIN_CLK = 1,
	PLLA_CLK = 2,
	UPLL_CLK = 3,
	NOT_SPEC = 4
}xk32_clock_name;

typedef struct xk32_clock{
	uint8_t is_initialized;
	uint32_t speed_is;
	uint32_t speed_should;
	uint8_t is_stable;
}xk32_clock;

void xk32_clock_init(struct xk32_clock *pself);
void xk32_clock_init_peripheral(struct xk32_clock *pself);
void xk32_clock_run(struct xk32_clock *pself);
void xk32_clock_change(struct xk32_clock *pself, uint32_t speed, uint8_t clk_select);
void xk32_clock_peripheral_connect(struct xk32_clock *pself, uint8_t peripheral, uint8_t switch_on_off);

#endif /* XK32_CLOCK_H_ */
