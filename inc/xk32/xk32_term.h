/*
 * term.h
 *
 * Created: 05.10.2018 18:26:34
 *  Author: vB1w
 */ 


#ifndef TERM_H_
#define TERM_H_

#include "sam.h"
#include <string.h>
#include "drv/atsame70q20/defs.h"
#include "drv/atsame70q20/uart.h"
#include "drv/atsame70q20/afec.h"
#include "if/if_led.h"

/* COMMANDS */
#define XK32_TERM_MSG_BEGINN	"$%B$"
#define XK32_TERM_MSG_END		"$%E$"
#define XK32_TERM_MSG_MSG		"MSG"
#define XK32_TERM_MSG_CMD		"CMD"
#define XK32_TERM_MSG_NULL		"\0"

#define XK32_TERM_BUF_MAX_MSG			0x0FU
#define XK32_TERM_BUF_MAX_MSG_LENGTH	0x0FU
#define XK32_TERM_CMD_MAX				0x0FU
#define XK32_TERM_MSG_LENGTH			0x0FU
#define XK32_TERM_MSG_ERRBUF			(XK32_TERM_BUF_MAX_MSG - 0x01U)
#define XK32_TERM_MSG_OVF				(XK32_TERM_BUF_MAX_MSG + 0x01U)

char tmp_num_buf[4];

typedef void (*task_function)( const void *pPara);
	
typedef struct xk32_term_msg_list{
	char *msg;
	uint8_t pos;
	struct xk32_term_msg_list *next;
}xk32_term_msg_list;
	
typedef struct xk32_term_msg{
	xk32_term_msg_list *msg_first;
	xk32_term_msg_list *msg_last;
	uint8_t b_overflow;
}xk32_term_msg;

typedef struct xk32_term_cmd_list{
	char *command;
	void *task_function;
	void *pPara;
	uint8_t b_active;
}xk32_term_cmd_list;

typedef struct xk32_term {
	uint8_t is_init;
	uint8_t is_driver_init;
	uint8_t is_driver_ok;
	uint8_t is_msg_overflow;
	uint32_t baudrate;
	uint8_t parity;
	uint32_t clock_in;
	xk32_term_msg *pmsg_out;
	xk32_term_msg *pmsg_in;
	char *pmsg_in_buf;
	xk32_term_cmd_list  *pmsg_cmd_list;
}xk32_term;
	
xk32_term s_xk32_term;

void xk32_term_set_init(xk32_term *pself, uint32_t baudrate, uint8_t parity);
void xk32_term_init(xk32_term *pself, uint32_t baud, uint8_t parity);
void xk32_term_run(xk32_term *pself);
void xk32_term_check(xk32_term *pself);

void xk32_term_on_irq(xk32_term *pself, uint32_t uart_sr);

void xk32_term_out_msg(char *msg);
void xk32_term_out_cmd(xk32_term *pself, uint8_t cmd_nr, void *arg0, void *arg1, void *arg2);
void xk32_term_out_nextline(void);
void xk32_term_out_tab(void);
void xk32_term_in_msg(xk32_term *pself, char *msg);
void xk32_term_in_cmd(xk32_term *pself, uint8_t cmd_nr, void *arg0, void *arg1, void *arg2);
//char *term_itoa(uint32_t value, uint8_t length);

void xk32_term_out_num(uint32_t num);


#endif /* TERM_H_ */
