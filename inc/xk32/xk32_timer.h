/*
 * timer.h
 *
 * Created: 27.07.2019 01:04:48
 *  Author: vB1w
 */ 


#ifndef TIMER_H_
#define TIMER_H_

#include "drv/atsame70q20/defs.h"

#include "sam.h"
#include "drv/atsame70q20/wdt.h"

#define TIMES_MAX 10

typedef void (*callback)( const void *pPara);

typedef struct dedicated_timer{
	uint8_t registered;
	uint16_t ctr;
	uint16_t to;
	callback fct;
	void *fct_para;
	uint8_t invoke;
}dedicated_timer;

typedef struct xk32_timer{
	const uint8_t ms;
	dedicated_timer *p_dedicated_timer;
	uint8_t times_ovflw;
	uint8_t irq_occured;
}xk32_timer;

struct dedicated_timer s_dedicated_timer[TIMES_MAX];
struct xk32_timer s_timer;

void xk32_timer_init(struct xk32_timer *pself);
void xk32_timer_update(struct xk32_timer *pself);
void xk32_timer_start(struct xk32_timer *pself, uint16_t time_ms, void *callback, void *cb_para);
void xk32_timer_on_irq(void);
void xk32_timer_callback_invocation(struct dedicated_timer *pself);

#endif /* TIMER_H_ */
