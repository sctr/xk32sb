/*
 * xk32_task.h
 *
 * Created: 19.01.2021 09:39:42
 *  Author: vB1w
 */ 


#ifndef XK32_TASK_H_
#define XK32_TASK_H_

#define TASK_MAX (uint8_t)0x0Fu

#include "sam.h"
#include "xk32/xk32_timer.h"

typedef void (*task_function)( const void *pPara);
	
typedef struct xk32_task_function{
	uint8_t registered;
	uint8_t enable;
	uint16_t unique_id;
	char *name;
	uint8_t is_running;
	task_function pfct;
	void *pPara;
}xk32_task_function;

typedef struct xk32_task_task{
	uint16_t ctr;
	uint32_t interval_ms;
	uint32_t is_interval_over;
	xk32_task_function *task_list;
	}xk32_task_task;

uint8_t xk32_task_register(struct xk32_task_task *pself, void *pfct, void *ppara, char *name);
void xk32_task_start(struct xk32_task_task *pself);
void xk32_task_stop(struct xk32_task_task *pself);
void xk32_task_enable(struct xk32_task_task *pself, char *name);
void xk32_task_disable(struct xk32_task_task *pself, char *name);
void xk32_task_update(struct xk32_task_task *pself); /*invoke in main while loop*/
void xk32_task_on_irq(struct xk32_task_task *pself);
void xk32_task_reload_interval(struct xk32_task_task *pself);
int8_t xk32_task_is_running(struct xk32_task_task *pself, char *name);

#endif /* XK32_TASK_H_ */