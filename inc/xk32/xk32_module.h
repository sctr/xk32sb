/*
 * xk32_module.h
 *
 * Created: 10.02.2021 18:42:18
 *  Author: vB1w
 */ 


#ifndef XK32_MODULE_H_
#define XK32_MODULE_H_

/*	TODO:: establish a frame for all modules that behave the same like init() and run()
	functions; this module should be used by all xk32 modules and upper layers e.g.
	application layer or interface layer;
*/

#include "sam.h"

typedef void (*module_function)( const void *pPara);

typedef struct xk32_module {
	uint8_t is_init;
	uint8_t enable;
	module_function init;
	module_function run;
	void *pself;
	}xk32_module;

void mod_run(void *pself);
void mod_init(void *pself);

#endif /* XK32_MODULE_H_ */