/*
 * com_msg_list.h
 *
 * Created: 07.09.2021 13:04:42
 *  Author: vB1w
 */ 


#ifndef COM_MSG_LIST_H_
#define COM_MSG_LIST_H_

#include "sam.h"

#define COM_MSG_TOTAL		20
#define COM_MSG_LENGTH_MAX	30

typedef struct com_msg {
	char *msg;
	}com_msg;
	
typedef struct com_msg_list{
	char *msg;
	uint8_t	enabled;
	uint8_t length_max;
}com_msg_list;

struct com_msg_list s_com_msg_list_in[COM_MSG_TOTAL];
struct com_msg_list s_com_msg_list_out[COM_MSG_TOTAL];

#endif /* COM_MSG_LIST_H_ */