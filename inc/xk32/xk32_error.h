/*
 * xk32_error.h
 *
 * Created: 12.02.2021 11:04:30
 *  Author: vB1w
 */ 


#ifndef XK32_ERROR_H_
#define XK32_ERROR_H_

#include "sam.h"

typedef struct xk32_error{
		uint8_t is_init;
		uint8_t status;
		/* TODO:: error msg list*/
		
	}xk32_error;


void xk32_error_init(xk32_error *pself);
void xk32_error_run(xk32_error *pself);
void xk32_error_set(xk32_error *pself, char *msg, uint8_t module);

#endif /* XK32_ERROR_H_ */