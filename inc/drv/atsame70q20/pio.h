/*
 * pio.h
 *
 * Created: 21.06.2018 22:19:34
 *  Author: vB1w
 */ 


#ifndef PIO_H_
#define PIO_H_

#include "drv/atsame70q20/defs.h"
#include "if/if_switch.h"

#define LED0_MSK	0x08U
#define LED1_MSK	0x04U
#define LED2_MSK	0x02U
#define LED3_MSK	0x01U

/* TODO:: implement a callback opportunity, when pio is triggered input even output*/
typedef struct pio_list{
	const char *channel;	// name of channel in ascii
	char *name;				// which functionality on this pin
	uint8_t available;		// configured yes/ no
	uint8_t pio_endis;		// enable or disable pio (disable for alternate function)
	uint8_t io;				// input or output
	uint8_t pupd;			// pull up pull down
	uint8_t multidriver;
	uint8_t owrite;			// output write en/ dis
	uint8_t interrupt;		// interrupt en/ dis
	uint8_t	pin_state;		// pin state value
	uint8_t periph;			// which periperal
	uint8_t i_edge_sel;		// input edge or level detection
	uint8_t i_rehl_sel;		// input rising edge or high level
	uint8_t i_add_irq_mode;	// additional interrupt mode register
	uint8_t	b_registered;	// is this pin already initialized
	}pio_list;

typedef struct pio{
	char *name;				// name of pio in ascii
	Pio *node;				// catch adress node
	pio_list *list;		// definition list for pio functionality
	uint8_t max;			// how many channels available
	}pio;

struct pio_list s_pioa_list[PIOA_PIN_COUNT];
struct pio_list s_pioc_list[PIOC_PIN_COUNT];
struct pio_list s_piod_list[PIOD_PIN_COUNT];

struct pio s_pioa;
struct pio s_pioc;
struct pio s_piod;

void pio_init(struct pio *pio);
void pio_deinit(struct pio *pio);
void pio_change(
		struct pio *pio,
		 uint8_t pin_num,
		char *name,
		uint8_t pio_endis,
		uint8_t io,
		uint8_t pupd,
		uint8_t multidriver,
		uint8_t owrite,
		uint8_t interrupt,
		uint8_t default_val,
		uint8_t periph,
		uint8_t i_edge_sel,
		uint8_t i_rehl_sel,
		uint8_t i_add_irq_mode
		);
		
void pio_switch(struct pio *pself, uint8_t pin, uint8_t u8switch);
void pio_pioc_on_irq(uint32_t stat_val);
void pio_toggle_led(uint8_t led_mask);


#endif /* PIO_H_ */
