/*
 * sb_wdt.h
 *
 * Created: 01.10.2018 19:17:41
 *  Author: vB1w
 */ 


#ifndef SB_WDT_H_
#define SB_WDT_H_

#include "stdint.h"

void sb_wdt_init(uint32_t sb_wdt_time);

void sb_wdt_rst(void);

#endif /* SB_WDT_H_ */
