/*
 * uart.h
 *
 * Created: 21.06.2018 22:07:25
 *  Author: vB1w
 */ 


#ifndef UART_H_
#define UART_H_

#include "sam.h"
#include "drv/atsame70q20/defs.h"
#include "drv/atsame70q20/pmc.h"

#define UARTWAIT	150U
#define CTR			UARTWAIT
#define EVEN		0x00U
#define ODD			0x01U
#define SPACE		0x02U
#define MARK		0x03U
#define PARNO		0x04U

typedef struct uart{
	char		*name;
	Uart		*node;
	uint8_t		is_init;
	uint32_t	main_freq;
	uint8_t		main_freq_is_init;
	uint32_t	baudrate;
	uint8_t		parity_bit;
	uint16_t	clock_divisor;
	uint32_t	irq_stat_bits;
}uart;

/*public*/
void uart_0_init(uint32_t baud, uint8_t parity);
void uart_1_init(uint32_t baud, uint8_t parity);

uint8_t uart_0_check(void);
uint8_t uart_1_check(void);

uint32_t uart_0_rx_irq(void);
uint32_t uart_1_rx_irq(void);

uint32_t uart_0_tx_empty_irq(void);
uint32_t uart_1_tx_empty_irq(void);

uint8_t uart_is_init(void);

uint8_t uart_check(uart *pself);

void uart_set_transmit_val(char trans_val);
char uart_get_received_val(void);
void uart_get_irq_stat_bits(uart *pself);
uint32_t uart_tx_empty_irq(uart *pself);
uint32_t uart_rx_irq(uart *pself);

void uart_irq_callback(uart *pself);

/*private*/
void uart_init(uart *pself, uint32_t baud, uint8_t parity);


#endif /* UART_H_ */