/*
 * pmc.h
 *
 * Created: 23.06.2018 12:27:47
 *  Author: vB1w
 */ 


#ifndef PMC_H_
#define PMC_H_

#include "drv/atsame70q20/defs.h"

#define PMC_PID_PIOA	0xAu
#define PMC_PID_PIOB	0xBu
#define PMC_PID_PIOC	0xCu
#define PMC_PID_UART	0x07u
#define PMC_PID_RTT		0x03u
#define PMC_PID_AFEC0	0x1Du
#define PMC_PID_AFEC1	0x28u
#define PMC_PID_DACC	0x1Eu
#define PMC_PID_PWM0	0x1Fu
#define PMC_PID_PWM1	0x3Cu

#define PMC_SLOW_CLK	0x00U
#define PMC_MAIN_CLK	0x01U

#define PMC_INTERNAL_OSZILLATOR	0x00U
#define PMC_EXTERNAL_OSZILLATOR	0x01U

#define PMC_PLL_DISABLE 0x00U
#define PMC_PLL_ENABLE 0x01U

#define PMC_CRYSTAL_STARTUP_TIME_MAX 0x0FU

#define PMC_MAIN_RC_4M			4000000U
#define PMC_MAIN_RC_8M			8000000U
#define PMC_MAIN_RC_12M			12000000U
#define PMC_SLOW_CLK_32K		32000U		// 32kHz
#define PMC_SLOW_CRYSTAL		32768U

#define PMC_EXT_OSZILLATOR_16M	16000000U	// 16MHz
#define PMC_EXT_OSZILLATOR_12M	12000000U	// 12MHz
#define PMC_EXT_OSZILLATOR_10M	10000000U	// 10MHz

#define PMC_EXT_OSZILLATOR_TOLERANCE 500000U
#define PMC_EXT_OSZILLATOR_16M_MAX ((PMC_EXT_OSZILLATOR_16M) + (PMC_EXT_OSZILLATOR_TOLERANCE))


#define PMC_PLLA_160M			160000000U	// 160MHz
#define PMC_PLLA_MAX			180000000U	// 180MHz

typedef struct system_clock{
	char *name;
	uint8_t initialized;
	uint32_t speed_set;
	uint32_t speed_get;
		
}system_clock;
	
typedef struct peripheral_clock{
	char *name;
	uint8_t initialized;
	uint32_t speed_set;
	uint32_t speed_get;
	uint8_t peripheral_id;
	uint8_t clock_selection;
		
}peripheral_clock;

typedef struct programable_clock{
	char *name;
	uint8_t initialized;
	uint32_t speed_set;
	uint32_t speed_get;
	
}programable_clock;

typedef struct pll_clock{
	uint8_t		is_init;
	uint8_t		enable;
	uint8_t		speed_ok;
	uint32_t	speed_set;
	uint32_t	speed_get;
	uint32_t	speed_tolerance;
	uint32_t	speed_max;
	uint8_t		division;
	uint8_t		multiplication;
}pll_clock;

typedef struct main_clock{
	uint8_t is_init;
	uint8_t	source;
	uint8_t	speed_ok;
	uint32_t speed_set;
	uint32_t speed_get;
	uint32_t maskspeed;
	uint32_t speed_max;
	uint32_t speed_tolerance;
}main_clock;

typedef struct master_clock{
	uint8_t	is_init;
	uint8_t speed_ok;
	uint32_t speed_set;
	uint32_t speed_get;
	uint32_t speed_max;
	uint32_t speed_tolerance;
	uint8_t input_clock_selection;
}master_clock;

typedef struct crystal_clock{
	uint8_t is_init;
	uint8_t enable;
	uint8_t speed_ok;
	uint32_t speed_set;
	uint32_t speed_get;
	uint32_t speed_max;
	uint32_t speed_tolerance;
	uint8_t startup_time;
}crystal_clock;

typedef struct pmc{
	char				*name;
	Pmc					*node;
	uint8_t				is_init;
	uint32_t			clk_speed;
	uint32_t			clk_speed_tolerance;
	uint32_t			meas_speed;				/// INFO:: over main frequency measurement
	uint8_t				oszillator_name;	
	crystal_clock		*pcrystal_clk;
	master_clock		*pmaster_clk;
	programable_clock	*pprog_clk;
	peripheral_clock	*pperiph_clk;
	main_clock			*pmain_clk;
	pll_clock			*ppllx_clk;
}pmc;
	
struct pmc s_pmc;

void pmc_init(struct pmc *pself);
void pmc_init_peripheral_clk(uint8_t pmc_pid, uint8_t pmc_clk_sel);

void pmc_main_clk_init(struct pmc *pself);
void pmc_main_clk_check(struct pmc *pself);

void pmc_rc_init(pmc *pself);
void pmc_rc_check(pmc *pself);

void pmc_crystal_init(struct pmc *pself);
void pmc_crystal_check(struct pmc *pself);

void pmc_main_oscillator_select(struct pmc *pself, uint8_t oscillator_name);
void pmc_main_frequency_select(struct pmc *pself, uint32_t frq);


void pmc_plla_init(pmc *pself);
void pmc_plla_check(pmc *pself);
void pmc_plla_enable(struct pmc *pself, uint8_t u8endis);
void pmc_plla_calc(struct pmc *pself);

void pmc_master_clk_init(pmc *pself);
void pmc_master_clk_calc(pmc *pself);

/*public*/
void run(void);
void select_internal_oscillator_12mhz(void);
void select_external_oscillator_crystal(void);

uint32_t get_main_oscillator_frequency(void);
uint8_t is_main_oscillator_ok(void);

#endif /* PMC_H_ */
