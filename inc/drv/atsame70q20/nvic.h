/*
 * nvic.h
 *
 * Created: 21.06.2018 22:23:01
 *  Author: vB1w
 */ 


#ifndef NVIC_H_
#define NVIC_H_

#include "stdint.h"
#include "sam.h"
#include "drv/atsame70q20/pio.h"
#include "drv/atsame70q20/afec.h"
#include "drv/atsame70q20/rtt.h"
#include "xk32/xk32_term.h"

void PIOC_Handler(void);
void RTT_Handler(void);
void UART0_Handler(void);
void AFE0_Handler(void);
void AFE1_Handler(void);

void nvic_init(void);
void nvic_register_irq(uint8_t handler, void *pfunction, void *pParam);


#endif /* NVIC_H_ */