/*
 * afec.h
 *
 * Created: 14.10.2018 15:30:10
 *  Author: vB1w
 */ 


#ifndef SB_AFEC_H_
#define SB_AFEC_H_

#include "sam.h"
#include "drv/atsame70q20/defs.h"
#include "xk32/xk32_term.h"

#define CH_TOT	12
#define CH0		0
#define CH1		1
#define CH2		2
#define CH3		3
#define CH4		4
#define CH5		5
#define CH6		6
#define CH7		7
#define CH8		8
#define CH9		9
#define CH10	10
#define CH11	11

typedef struct afec_ch_list{
	uint8_t available;
	uint8_t newval;
	uint16_t value;
	char value_ascii[5];
	char *name_ch;
	char *name_function;
	uint16_t offset;
}afec_ch_list;

typedef struct afec{
	char *name;
	Afec *node;
	uint8_t prescale;
	uint16_t startuptime;
	struct afec_ch_list *ch_list;
	uint8_t u8_term_out_wait;
	}afec;

struct afec s_afec0;
struct afec s_afec1;

struct afec_ch_list s_afec0_ch[CH_TOT];
struct afec_ch_list s_afec1_ch[CH_TOT];

void afec_init(afec *pself);
void afec_run(afec *pself);
void afec_aoff(afec *pself, uint16_t ch, uint16_t offset);
void afec_start_conversion(afec *pself);
void afec_on_irq(afec *pself, uint32_t irq_val);
void afec_store_ch_val(afec *pself, uint8_t ch);
void afec_prepare4term_out(afec *pself, uint8_t ch);
void afec_itoa(afec *pself, uint8_t ch);
void afec_newval(afec *pself, uint8_t ch);

#endif /* SB_AFEC_H_ */
