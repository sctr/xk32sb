/*
 * sb_defs.h
 *
 * Created: 01.10.2018 17:48:57
 *  Author: vB1w
 */ 


#ifndef SB_DEFS_H_
#define SB_DEFS_H_

#include "sam.h"

#define SB_RTT_PRESCAL 0x00FFU
#define SB_RTT_VALUE 0x0000000FU
#define SB_WDT_VALUE 0x000FFFU

#define PERIPHERAL_CLOCK_MAX	31U
#define PROGRAMABLE_CLOCK_MAX	10U

#define SB_UART_BAUDRATE1M		1000000U
#define SB_UART_BAUDRATE921600	921600U	
#define SB_UART_BAUDRATE38K		38400U

#define PIOA_PIN_COUNT			0x20u //0d32u
#define PIOC_PIN_COUNT			0x20u //0d32u
#define PIOD_PIN_COUNT			0x20u //0d32u

#define bTRUE				0x01u
#define bFALSE				0x00u

#define YES					0x01u
#define NO					0x00u

#define ON					0x01u
#define OFF					0x00u

#define ENABLE				0x01u
#define DISABLE				0x00u

#define INPUT				0x00u
#define OUTPUT				0x01u

#define PULLDOWN			0x00u
#define PULLUP				0x01u

#define PERIPHX				0x00u
#define PERIPHA				0x01u
#define PERIPHB				0x02u
#define PERIPHC				0x03u
#define PERHIPD				0x04u

#define EDGE				0x00u
#define LEVEL				0x01u

#define RISINGHIGH			0x01u
#define FALLINGLOW			0x00u


#endif /* SB_DEFS_H_ */
