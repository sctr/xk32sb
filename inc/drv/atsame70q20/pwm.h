/*
 * sb_pwm.h
 *
 * Created: 18.10.2018 21:24:57
 *  Author: vB1w
 */ 


#ifndef SB_PWM_H_
#define SB_PWM_H_

#include "sam.h"

typedef struct sb_pwm{
	char		*name;
	Pwm			*node;
	uint16_t	frequency_pwm;
	uint32_t	frequency_inclock;
	
}sb_pwm;

struct sb_pwm s_sb_pwm0;

void sb_pwm_init(sb_pwm *pself);
void sb_pwm_set_frequency(sb_pwm *pself, uint16_t freq);
void sb_pwm_set_duty(sb_pwm *pself, uint16_t duty);
void sb_pwm_enable(sb_pwm *pself);
void sb_pwm_disable(sb_pwm *pself);

#endif /* SB_PWM_H_ */