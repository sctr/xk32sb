/*
 * sb_rtt.h
 *
 * Created: 01.10.2018 17:26:18
 *  Author: vB1w
 */ 


#ifndef SB_RTT_H_
#define SB_RTT_H_

#include "stdint.h"
#include "drv/atsame70q20/afec.h"

typedef void (* irq_callback)();

typedef struct sb_rtt{
	uint16_t prescaler;
	uint32_t counter;
	uint8_t clock;
	irq_callback fct;
}sb_rtt;

uint8_t rtt_sec_over;

void sb_rtt_init(uint8_t clock_select, uint32_t sb_rtt_value, uint16_t sb_rtt_prescaler, void *irq_callback);
void sb_rtt_enable_irq(void);
void sb_rtt_enable_irq_global(void);
void sb_rtt_set_val(uint32_t sb_rtt_value);
uint32_t sb_rtt_get_current_ctr(void);
void sb_rtt_on_irq(uint32_t irq_val);

#endif /* SB_RTT_H_ */
