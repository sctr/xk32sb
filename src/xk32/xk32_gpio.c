/*
 * gpio.c
 *
 * Created: 27.07.2019 21:42:41
 *  Author: vB1w
 */ 

#include "xk32/xk32_gpio.h"


void xk32_gpio_register_led(uint8_t port, uint8_t pin_num, char *purpose){
	/* declare parameter for pio list*/
	xk32_gpio_overwrite(port, pin_num, purpose, ENABLE, OUTPUT, PULLDOWN, DISABLE, ENABLE, DISABLE, 0x01U, PERIPHX, EDGE, RISINGHIGH, ENABLE);
}

void xk32_gpio_register_switch(uint8_t port, uint8_t pin_num, char *purpose){
	/* callback*/
}

void xk32_gpio_register_pio(uint8_t port, uint8_t pin_num, char *purpose){
	/* callback when input*/
}

void xk32_gpio_set(uint8_t port, uint8_t pin_num, uint8_t led_switch, char *purpose){
	xk32_gpio_overwrite(port, pin_num, purpose, ENABLE, OUTPUT, PULLDOWN, DISABLE, ENABLE, DISABLE, led_switch, PERIPHX, EDGE, RISINGHIGH, ENABLE);
}

void xk32_gpio_overwrite(
		uint8_t port,
		uint8_t pin_num,
		char *pfct,
		uint8_t pio_endis,
		uint8_t io,
		uint8_t pupd,
		uint8_t multidriver,
		uint8_t owrite,
		uint8_t interrupt,
		uint8_t default_val,
		uint8_t periph,
		uint8_t i_edge_sel,
		uint8_t i_rehl_sel,
		uint8_t i_add_irq_mode
		){
	pio *pio;
	switch(port){
	case PORTA:{
		pio = &s_pioa;
		break;
	}
	case PORTC:{
		pio = &s_pioc;
		break;
	}
	case PORTD:{
		pio = &s_piod;
		break;
	}
	default:{
		pio = 0x00U;
		break;
	}
	}
	if(pio != 0x00U){
		pio_change(pio, pin_num, pfct, pio_endis,  io, pupd,  multidriver, owrite, interrupt, default_val, periph, i_edge_sel, i_rehl_sel, i_add_irq_mode);	
	}
	
}
