/*
 * xk32_task.c
 *
 * Created: 19.01.2021 09:39:22
 *  Author: vB1w
 */ 

#include "xk32/xk32_task.h"

/** this update routine must be invoked in main continuously
	it invokes the previously registered tasks
**/
void xk32_task_update(struct xk32_task_task *pself){
	/* check in infinite while loop if task interval (here 1ms) is over*/
	if(pself->is_interval_over == bTRUE){
		/*invoke task functions from task list in given order*/
			for(uint8_t tasknr = 0; tasknr < TASK_MAX; tasknr++){
				if(pself->task_list[tasknr].enable == bTRUE && pself->task_list[tasknr].registered == bTRUE){
					pself->task_list[tasknr].is_running = bTRUE;
					pself->task_list[tasknr].pfct(pself->task_list[tasknr].pPara);
					pself->task_list[tasknr].is_running = bFALSE;
				}
			}
		pself->is_interval_over = bFALSE;
	}
}

uint8_t xk32_task_register(struct xk32_task_task *pself, void *pfct, void *ppara, char *name){
/* TODO:: HappyPath still, later with return value*/
/* TODO:: "unique id" must be set randomly and checked if already existing, currently it is same as counter*/

	for(uint8_t tasknr = 0; tasknr < TASK_MAX; tasknr++){
		if (pself->task_list[tasknr].registered != bTRUE){
			pself->task_list[tasknr].pfct = pfct;
			pself->task_list[tasknr].pPara = ppara;
			pself->task_list[tasknr].name = name;
			pself->task_list[tasknr].unique_id = tasknr;
			pself->task_list[tasknr].registered = bTRUE;
			pself->task_list[tasknr].enable = bTRUE;
			pself->ctr++;
			return 0u;
		}
	}
	/* TODO:: task overflow check() implementation*/
}

void xk32_task_start(struct xk32_task_task *pself){
	xk32_timer_start(&s_timer, pself->interval_ms, xk32_task_on_irq, pself);
}

void xk32_task_stop(struct xk32_task_task *pself){
	
}

void xk32_task_enable(struct xk32_task_task *pself, char *name){
	for(uint8_t i=0; i<pself->ctr; i++){
		if(pself->task_list[i].name == name){ /* TODO:: insert strcmp()*/
			pself->task_list[i].enable = bTRUE;
		}
	}
}

void xk32_task_disable(struct xk32_task_task *pself, char *name){
	for(uint8_t i = 0; i < pself->ctr; i++){
		if(pself->task_list[i].name == name){ /* TODO:: insert strcmp()*/
			pself->task_list[i].enable = bFALSE;
		}
	}
}

void xk32_task_on_irq(struct xk32_task_task *pself){
	pself->is_interval_over = bTRUE;
	xk32_task_reload_interval(pself);
}

void xk32_task_reload_interval(struct xk32_task_task *pself){
	xk32_timer_start(&s_timer, pself->interval_ms, xk32_task_on_irq, pself);
}

int8_t xk32_task_is_running(struct xk32_task_task *pself, char *name){
	uint8_t b_ret = bFALSE;
	for(uint8_t i=0; i<pself->ctr; i++){
		if(pself->task_list[i].name == name){ /* TODO:: insert strcmp()*/
			b_ret = pself->task_list[i].is_running;
		}
	}
	return b_ret;
}