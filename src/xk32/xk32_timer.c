/*
 * timer.c
 *
 * Created: 27.07.2019 01:04:33
 *  Author: vB1w
 */ 

#include "xk32/xk32_timer.h"

struct dedicated_timer s_dedicated_timer[TIMES_MAX];

struct xk32_timer s_timer = {
	.ms = 1,
	.p_dedicated_timer = &s_dedicated_timer,
	.times_ovflw = bFALSE,
	.irq_occured = bFALSE
	};
 
void xk32_timer_init(xk32_timer *pself){
	uint8_t prescaler = 32U;
	uint8_t counter = 0U;
	uint8_t clock = 0U; // 32kHz s
	/* --> 1ms irq takt*/
	sb_rtt_init(clock, counter, prescaler, xk32_timer_on_irq);
}

void xk32_timer_update(xk32_timer *pself){
	for(uint8_t reg = 0; reg < TIMES_MAX; reg++){
		if(pself->p_dedicated_timer[reg].registered == bTRUE){
			if(pself->irq_occured == bTRUE){
				pself->p_dedicated_timer[reg].ctr++;
			}
			if(pself->p_dedicated_timer[reg].ctr >= pself->p_dedicated_timer[reg].to){
				xk32_timer_callback_invocation(&pself->p_dedicated_timer[reg]);
				pself->p_dedicated_timer[reg].registered = bFALSE;
			}
		}
	}
	pself->irq_occured = bFALSE;
}

void xk32_timer_start(xk32_timer *pself, uint16_t time_ms, void *callback, void *cb_param){
	for(uint8_t reg = 0; reg < TIMES_MAX; reg++){
		if (pself->p_dedicated_timer[reg].registered != bTRUE){
			pself->p_dedicated_timer[reg].registered = bTRUE;
			pself->p_dedicated_timer[reg].ctr = 0x00U;
			pself->p_dedicated_timer[reg].to = time_ms;
			pself->p_dedicated_timer[reg].fct = callback;
			pself->p_dedicated_timer[reg].fct_para = cb_param;
			pself->p_dedicated_timer[reg].invoke = bFALSE;
			break;
		}
		if (reg == TIMES_MAX){
			/* indicate overflow*/
		}
	}
}

void xk32_timer_on_irq(void){
	for(uint8_t reg = 0; reg < TIMES_MAX; reg++){
		if(s_timer.p_dedicated_timer[reg].registered == bTRUE){
			s_timer.p_dedicated_timer[reg].ctr++;
			if(s_timer.p_dedicated_timer[reg].ctr >= s_timer.p_dedicated_timer[reg].to){
				xk32_timer_callback_invocation(&s_timer.p_dedicated_timer[reg]);
				s_timer.p_dedicated_timer[reg].registered = bFALSE;
			}
		}
	}
}

/* NOTE::	do not invoke callback in irq
			irq must be as short as possible*/
void xk32_timer_callback_invocation(dedicated_timer *pself){
	pself->fct(pself->fct_para);
	pself->registered = bFALSE;
}
