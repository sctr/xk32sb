/*
 * term.c
 *
 * Created: 05.10.2018 18:30:20
 *  Author: vB1w
 */ 

#include "xk32/xk32_term.h"

xk32_term_msg_list s_term_msg_list_out[XK32_TERM_BUF_MAX_MSG];
xk32_term_msg_list s_term_msg_list_in[XK32_TERM_BUF_MAX_MSG];

char *buf_msg_in[XK32_TERM_BUF_MAX_MSG_LENGTH];

xk32_term_cmd_list s_term_cmd_list[XK32_TERM_CMD_MAX] = {
	{"led0_on", &if_led0_on, &s_if_led0, 0},
	{"led0_off", &if_led0_off, &s_if_led0, 0},
	{"step_start", &sb_mot_ctrl_cmd_start, &s_sb_mot_ctrl, 0},
	{"step_stop", &sb_mot_ctrl_cmd_stop, &s_sb_mot_ctrl, 0}
};

xk32_term_msg s_term_msg_out = {
	&s_term_msg_list_out[0x00].msg,
	0x00u,
	0x00u
};

xk32_term_msg s_term_msg_in = {
	&s_term_msg_list_in[0x00].msg,
	0x00u,
	0x00u
};

xk32_term s_xk32_term = {
	.is_init = bFALSE,
	.is_driver_init = bFALSE,
	.is_msg_overflow = bFALSE,
	.baudrate = 0x00U,
	.parity = 0x00U,
	.clock_in = 0x00U,
	.pmsg_out = &s_term_msg_out,
	.pmsg_in = &s_term_msg_in,
	.pmsg_in_buf = &buf_msg_in,
	.pmsg_cmd_list = &s_term_msg_list_in /* TODO :: command list from app*/
	};
	
void xk32_term_init(xk32_term *pself, uint32_t baud, uint8_t parity){
	/*	TODO:: pointer for uart_struct over void loop through from sb_app ?
		is a architecture requirement question
	*/
	pself->baudrate = baud;
	pself->parity = parity;
	
	/*init message buffer*/
	for(int i = 0x00u; i< XK32_TERM_BUF_MAX_MSG; i++){
		s_term_msg_list_out[i].next = &s_term_msg_list_out[i+1];
		s_term_msg_list_out[i].pos = 0x00u;
		s_term_msg_list_out[i].msg = XK32_TERM_MSG_NULL;
	}
	s_term_msg_list_out[XK32_TERM_MSG_ERRBUF].next = &s_term_msg_list_out;
	s_term_msg_out.msg_first = &s_term_msg_list_out;
	s_term_msg_out.msg_last = &s_term_msg_list_out[0x01u];
	s_term_msg_out.msg_first->msg = "";
	s_term_msg_out.b_overflow = NO;
	
	pself->is_init = bTRUE;
}

void xk32_term_run(xk32_term *pself){
	
	if(pself->is_init == bTRUE){

		/*init when freq_in is ok*/
		pself->is_driver_init = uart_is_init(); 
		if(pself->is_driver_init == bFALSE){
			uart_0_init(pself->baudrate, pself->parity); /* TODO:: separate drv from xk32; definition of connector functions? */
		}
		else{
			/*TODO:: check if msg out must be written*/
			
			/*TODO:: cmd vs. msg comparison here for incomming msg*/
			
		}
	}
}

void xk32_term_check(xk32_term *pself){
	/* term check buffer overflow*/
	if(pself->is_msg_overflow == bTRUE){
		/* TODO:: send error msg in error msg buffer*/
	}
	
	pself->is_driver_ok = uart_0_check();
}

void xk32_term_out_msg(char *msg){
	/*TODO:: add msg header*/
	if(s_term_msg_out.b_overflow == NO){
		if(s_term_msg_out.msg_last->msg[s_term_msg_out.msg_last->pos] == '\0'){
			s_term_msg_out.msg_last->msg = msg;
			s_term_msg_out.msg_last->pos = 0x00u;
			s_term_msg_out.msg_last = s_term_msg_out.msg_last->next;
			NVIC_EnableIRQ(UART0_IRQn);
		}
		else{
			s_term_msg_out.b_overflow == YES;
		}
	}
}

/*TODO:: create cmd list (out and in) and export when compiled for xkGUI*/
void xk32_term_out_cmd(xk32_term *pself, uint8_t cmd_nr, void *arg0, void *arg1, void *arg2){
	/*TODO:: add cmd header*/
	
}

void xk32_term_out_tab(void){
	xk32_term_out_msg("\r");
}

void xk32_term_out_nextline(void){
	xk32_term_out_msg("\n");
}

void xk32_term_in_msg(xk32_term *pself, char *msg){
	/*sort message due to input command specification in header*/
	
	/*put message to msg_in buffer when full command read*/
	
}

void xk32_term_in_cmd(xk32_term *pself, uint8_t cmd_nr, void *arg0, void *arg1, void *arg2){
	
	
}

//char *term_itoa(uint32_t value, uint8_t length){
	//char *ret = (char *) malloc(length +1 * sizeof(char));
	//uint8_t i;
	//for(i=0; i<length; i++){
		//ret[length- i- 1] = (value % 10)+ 48;
		//value = value/ 10;
	//}
	//ret[length]='\0';
	//return ret;
//}

void xk32_term_out_num(uint32_t num){
	xk32_term_out_msg(tmp_num_buf);
}

void xk32_term_on_irq(xk32_term *pself, uint32_t uart_sr){

	//uart_get_irq_stat_bits();
	
	if(uart_0_tx_empty_irq()){
		
		uint8_t i = 0x00u;
		uint8_t buf_empty = YES;
		while(i < XK32_TERM_BUF_MAX_MSG){
			if(s_term_msg_list_out[i].msg[s_term_msg_list_out[i].pos] != '\0'){
					buf_empty = NO;
					i = XK32_TERM_BUF_MAX_MSG;
			}
			else{
				i++;
			}
		}
		if(buf_empty == YES){
			NVIC_DisableIRQ(UART0_IRQn);
		}
		else
		{
			if(s_term_msg_out.msg_first->msg[s_term_msg_out.msg_first->pos] == '\0'){
				s_term_msg_out.msg_first = s_term_msg_out.msg_first->next;
			}
			uart_set_transmit_val(s_term_msg_out.msg_first->msg[s_term_msg_out.msg_first->pos]);
			s_term_msg_out.msg_first->pos++;
		}
	}
	if(uart_0_rx_irq()){
		/* TODO:: read rx buffer*/
		strncat(pself->pmsg_in_buf, uart_get_received_val(), 1u);
		/*check if a message is in pmsg_in_buf*/
		if(strlen(pself->pmsg_in_buf)>= XK32_TERM_BUF_MAX_MSG_LENGTH){
			uint8_t get_position = strchr(pself->pmsg_in_buf, '%');
			
		}
		
	}
}
