/*
 * clock.c
 *
 * Created: 06.08.2019 23:49:45
 *  Author: vB1w
 */ 

#include "xk32/xk32_clock.h"

void xk32_clock_init(xk32_clock *pself){

	pmc_init(&s_pmc);

}

void xk32_clock_init_peripheral(xk32_clock *pself){
	/* TODO:: setup table in sb_pm*/
	pmc_init_peripheral_clk(PMC_PID_PIOA, PMC_MAIN_CLK);
	pmc_init_peripheral_clk(PMC_PID_PIOC, PMC_MAIN_CLK);
	pmc_init_peripheral_clk(PMC_PID_UART, PMC_MAIN_CLK);
	pmc_init_peripheral_clk(PMC_PID_DACC, PMC_MAIN_CLK);
	pmc_init_peripheral_clk(PMC_PID_AFEC0, PMC_MAIN_CLK);
	pmc_init_peripheral_clk(PMC_PID_AFEC1, PMC_MAIN_CLK);
	pmc_init_peripheral_clk(PMC_PID_PWM0, PMC_MAIN_CLK);
	pmc_init_peripheral_clk(PMC_PID_PWM1, PMC_MAIN_CLK);

}

void xk32_clock_run(xk32_clock *pself){
	/* is initialized*/
	/* check clocks*/
	pmc_run(&s_pmc);
}

void xk32_clock_set_speed(uint32_t speed){
	
}

void xk32_clock_change(xk32_clock *pself, uint32_t speed, uint8_t clk_select){
	
}

void xk32_clock_peripheral_connect(struct xk32_clock *pself, uint8_t peripheral, uint8_t switch_on_off){

}
