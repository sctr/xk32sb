/*
 * sb_rtt.c
 *
 * Created: 01.10.2018 17:26:03
 *  Author: vB1w
 */ 
#include "drv/atsame70q20/rtt.h"

uint8_t rtt_sec_over = 0x00U;
uint8_t su8i = 0x00U;

struct sb_rtt s_sb_rtt;

void sb_rtt_init(uint8_t clock_select, uint32_t sb_rtt_value, uint16_t sb_rtt_prescaler, void *irq_callback){

	s_sb_rtt.prescaler = sb_rtt_prescaler;
	s_sb_rtt.counter = sb_rtt_value;
	s_sb_rtt.clock = clock_select;
	s_sb_rtt.fct = irq_callback;

	RTT->RTT_MR = 0x00000000U; // ? reset mr is required
	RTT->RTT_MR |= RTT_MR_RTPRES(sb_rtt_prescaler);
	sb_rtt_set_val(sb_rtt_value);
	sb_rtt_enable_irq();
	RTT->RTT_MR |= RTT_MR_RTTRST;
}

void sb_rtt_enable_irq(void){
	RTT->RTT_MR |= RTT_MR_ALMIEN;
}

void sb_rtt_enable_irq_global(void){

}

void sb_rtt_set_val(uint32_t sb_rtt_value){
	RTT->RTT_AR	= sb_rtt_value;
}

uint32_t sb_rtt_get_current_ctr(void){
	return RTT->RTT_VR;
}

void sb_rtt_on_irq(uint32_t irq_val){
	
	uint8_t new_irq_val = irq_val & RTT_SR_ALMS;
	if((new_irq_val) == 0x01U){
		s_sb_rtt.fct();
//		pio_toggle_led(LED1_MSK);
//		if(bswitch_phase == 0x00u){
//				sb_mot_ctrl_turn_off();
//			}
//
//			if(bswitch_phase == 0x01u){
//				sb_mot_ctrl_next_phase();
//				bswitch_phase = 0x00u;
//			}
//
//			if(bload_highside == 0x00u){
//				sb_mot_ctrl_turn_off();
//			}
//
//			if(bload_highside == 0x01u){
//				sb_mot_ctrl_low_side_all();
//				bload_highside = 0x00u;
//			}
			/* start afec conversation ch0 + ch1*/
			if(su8i > 20){
				afec_start_conversion(&s_afec0);
				afec_start_conversion(&s_afec1);
			}
			else{
				su8i++;
			}
/*			rtt_sec_over++;*/
}
	RTT->RTT_MR |= RTT_MR_RTTRST;
}
