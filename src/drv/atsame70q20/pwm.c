/*
 * sb_pwm.c
 *
 * Created: 18.10.2018 21:25:26
 *  Author: vB1w
 */ 

#include "drv/atsame70q20/pwm.h"

/* Pinconfig:
Name	Pin		Peripheral	Modulename

UH		PA0		A			PWMC0_PWMH0
UL		PA2		A			PWMC0_PWMH1
VH		PD1		B			PWMC1_PWMH0
VL		PD3		B			PWMC1_PWMH1
WH		PD5		B			PWMC1_PWMH2
WL		PD7		B			PWMC1_PWMH3

*/
sb_pwm s_sb_pwm0 = {
	.name = "PWM0",
	.node = ((Pwm	*)0x40020000U),
	.frequency_pwm = 12000U,
	.frequency_inclock = 0U
};

void sb_pwm_init(sb_pwm *pself){
	PWM0->PWM_CLK |= PWM_CLK_DIVA(0x01u) | PWM_CLK_DIVB(0x01u); // enable clk
	PWM1->PWM_CLK |= PWM_CLK_DIVA(0x01u) | PWM_CLK_DIVB(0x01u); // enable clk

	PWM0->PWM_CH_NUM[1].PWM_CMR |= PWM_CMR_CALG | PWM_CMR_CES | (0x01u << 0) | (0x01u << 16);
	PWM1->PWM_CH_NUM[1].PWM_CMR |= PWM_CMR_CALG | PWM_CMR_CES | (0x01u << 0) | (0x01u << 16);
	PWM1->PWM_CH_NUM[3].PWM_CMR |= PWM_CMR_CALG | PWM_CMR_CES | (0x01u << 0) | (0x01u << 16);
	sb_pwm_set_frequency(pself, 0x00AFu);
	sb_pwm_set_duty(pself, 0x00ACu);
	PWM0->PWM_DIS |= PWM_DIS_CHID1;	//PA2 0PWMH1
	PWM1->PWM_DIS |= PWM_DIS_CHID1| PWM_DIS_CHID3; //PD3 1PWMH1 | PD7 1PWMH3
}

void sb_pwm_set_frequency(sb_pwm *pself, uint16_t freq){
		PWM0->PWM_CH_NUM[1].PWM_CPRD = freq;
		PWM1->PWM_CH_NUM[1].PWM_CPRD = freq;
		PWM1->PWM_CH_NUM[3].PWM_CPRD = freq;
}

void sb_pwm_set_duty(sb_pwm *pself, uint16_t duty){
		PWM0->PWM_CH_NUM[1].PWM_CDTY = duty;
		PWM1->PWM_CH_NUM[1].PWM_CDTY = duty;
		PWM1->PWM_CH_NUM[3].PWM_CDTY = duty;
}

void sb_pwm_enable(sb_pwm *pself){
	
}

void sb_pwm_disable(sb_pwm *pself){
	
}

//void sb_pwm_on_switch_load_cap(){ // for test case
	//sb_pwm_step_man_trigger_all_low_side();
	////if(PWM0->PWM_SR & PWM_SR_CHID1){
		////PWM0->PWM_DIS |= PWM_ENA_CHID1;	//PA2 0PWMH1
		////PWM1->PWM_DIS |= PWM_ENA_CHID1| PWM_ENA_CHID3; //PD3 1PWMH1 | PD7 1PWMH3
	////}
	////else{
		////PWM0->PWM_ENA |= PWM_ENA_CHID1;	//PA2 0PWMH1
		////PWM1->PWM_ENA |= PWM_ENA_CHID1| PWM_ENA_CHID3; //PD3 1PWMH1 | PD7 1PWMH3
	////}
//}

