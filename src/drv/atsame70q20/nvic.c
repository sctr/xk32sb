/*
 * nvic.c
 *
 * Created: 21.06.2018 22:23:32
 *  Author: vB1w
 */ 

#include "drv/atsame70q20/nvic.h"

void nvic_register_irq(uint8_t handler, void *pfunction, void *pParam){
	
}

void PIOA_Handler(void){
		NVIC_DisableIRQ(PIOA_IRQn);
		uint32_t ivalue = PIOA->PIO_ISR;
		pio_pioa_on_irq(ivalue);
		NVIC_EnableIRQ(PIOA_IRQn);
	
}

void PIOC_Handler(void){
	NVIC_DisableIRQ(PIOC_IRQn);
	uint32_t ivalue = PIOC->PIO_ISR;
	pio_pioc_on_irq(ivalue);
	NVIC_EnableIRQ(PIOC_IRQn);
}

void RTT_Handler(void){
	uint32_t ivalue = RTT->RTT_SR;
	sb_rtt_on_irq(ivalue);
}

void UART0_Handler(void){
	uint32_t ivalue = UART0->UART_SR;
	xk32_term_on_irq(&s_xk32_term, ivalue);
}

void AFEC0_Handler(void){
	uint32_t ivalue = AFEC0->AFEC_ISR;
	afec_on_irq(&s_afec0, ivalue);
}

void AFEC1_Handler(void){
	uint32_t ivalue = AFEC1->AFEC_ISR;
	afec_on_irq(&s_afec1, ivalue);
}
void nvic_init(void){
	NVIC_DisableIRQ(WDT_IRQn);
	NVIC_DisableIRQ(PIOA_IRQn);
	NVIC_EnableIRQ(PIOC_IRQn);
	NVIC_EnableIRQ(RTT_IRQn);
	NVIC_EnableIRQ(AFEC0_IRQn);
	NVIC_EnableIRQ(AFEC1_IRQn);
}

