/*
 * pio.c
 *
 * Created: 21.06.2018 22:19:47
 *  Author: vB1w
 */ 

#include "drv/atsame70q20/pio.h"

pio_list s_pioa_list[PIOA_PIN_COUNT] = {
	/*channel	name		avail	pioen/dis	io			pupd		mulitdriv	owrite		interrupt	state	periph		edge sel	rehl sel	add irq mode	registered*/
	{"CH00",	"PWMUH",	YES,	ENABLE,		OUTPUT,		PULLDOWN,	DISABLE,	ENABLE,		DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH01",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH02",	"PWMUL",	YES,	DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHA,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH03",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH04",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH05",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH06",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH07",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH08",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH09",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH10",	"UART_TX",	YES,	DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH11",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH12",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH13",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE,	 		bFALSE},
	{"CH14",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH15",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH16",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH17",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH18",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH19",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH20",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH21",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH22",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH23",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH24",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH25",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH26",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH27",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH28",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH29",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH30",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH31",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE}
};

pio s_pioa = {
	"PIOA",
	 ((Pio    *)0x400E0E00U),
	 s_pioa_list,
	 PIOA_PIN_COUNT
};

pio_list s_pioc_list[PIOC_PIN_COUNT] = {
	/*channel	name		avail	pioen/dis	io			pupd		mulitdriv	owrite		interrupt	state	periph		edge sel	rehl sel	add irq mode	registered*/
	{"CH00",	"LED0",		YES,	ENABLE,		OUTPUT,		PULLDOWN,	DISABLE,	ENABLE,		DISABLE,	0x01u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH01",	"LED1",		YES,	ENABLE,		OUTPUT,		PULLDOWN,	DISABLE,	ENABLE,		DISABLE,	0x01u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH02",	"LED2",		YES,	ENABLE,		OUTPUT,		PULLDOWN,	DISABLE,	ENABLE,		DISABLE,	0x01u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH03",	"LED3",		YES,	ENABLE,		OUTPUT,		PULLDOWN,	DISABLE,	ENABLE,		DISABLE,	0x01u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH04",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH05",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH06",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH07",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH08",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH09",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH10",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH11",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH12",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH13",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH14",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH15",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH16",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH17",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH18",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH19",	"SWTCH0",	YES,	ENABLE,		INPUT,		PULLDOWN,	DISABLE,	DISABLE,	ENABLE,		0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH20",	"SWTCH1",	YES,	ENABLE,		INPUT,		PULLDOWN,	DISABLE,	DISABLE,	ENABLE,		0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH21",	"SWTCH2",	YES,	ENABLE,		INPUT,		PULLDOWN,	DISABLE,	DISABLE,	ENABLE,		0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH22",	"SWTCH3",	YES,	ENABLE,		INPUT,		PULLDOWN,	DISABLE,	DISABLE,	ENABLE,		0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH23",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH24",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH25",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH26",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH27",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH28",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH29",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH30",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH31",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE}
};

pio s_pioc = {
	"PIOC",
	 ((Pio    *)0x400E1200U),
	 s_pioc_list,
	 PIOC_PIN_COUNT
};

pio_list s_piod_list[PIOC_PIN_COUNT] = {
	/*channel	name		avail	pioen/dis	io			pupd		mulitdriv	owrite		interrupt	state	periph		edge sel	rehl sel	add irq mode	registered*/
	{"CH00",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH01",	"PWMVH",	YES,	ENABLE,		OUTPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH02",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH03",	"PWMVL",	YES,	DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHB,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH04",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH05",	"PWMWH",	YES,	ENABLE,		OUTPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH06",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH07",	"PWMWL",	YES,	DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHB,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH08",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH09",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH10",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH11",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH12",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH13",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH14",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH15",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH16",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH17",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH18",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH19",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH20",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH21",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH22",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH23",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH24",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH25",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH26",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH27",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH28",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH29",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH30",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE},
	{"CH31",	"n/a",		NO,		DISABLE,	INPUT,		PULLDOWN,	DISABLE,	DISABLE,	DISABLE,	0x00u,	PERIPHX,	EDGE,		RISINGHIGH,	ENABLE, 		bFALSE}
};

pio s_piod = {
	"PIOD",
	 ((Pio    *)0x400E1400U),
	 s_piod_list,
	 PIOD_PIN_COUNT
};

/* TODO:: implement a callback opportunity, when pio is triggered input even output*/
void pio_init(struct pio *pio){
	for(uint8_t i = 0; i < pio->max; i++){
		if(pio->list[i].available == YES && pio->list[i].b_registered != bTRUE){
			if(pio->list[i].pio_endis == ENABLE){
				if(pio->list[i].pupd == PULLUP){
					pio->node->PIO_PUER |= (0x01u << i);
				}
				else
				{
					pio->node->PIO_PPDER|= (0x01u << i);
				}
				if(pio->list[i].io == INPUT){
					//input (switches)
					pio->node->PIO_ODR |= (0x01u << i);
					//set input parameter edge slection...
					if(pio->list[i].i_edge_sel == EDGE){
						pio->node->PIO_ESR |= (0x01u << i);
					}
					else
					{
						pio->node->PIO_ESR &= (0x01u << i);
					}
					if(pio->list[i].i_rehl_sel == RISINGHIGH){
						pio->node->PIO_REHLSR |= (0x01u << i);
					}
					else{
						pio->node->PIO_REHLSR &= ~(0x01u << i);
					}
					if(pio->list[i].i_add_irq_mode == ENABLE){
						pio->node->PIO_AIMER |= (0x01u << i);
					}
					else
					{
						pio->node->PIO_AIMDR |= (0x01u << i);						
					}
				}
				else
				{
					// output (leds, ..)
					pio->node->PIO_OER |= (0x01u << i);
					if(pio->list[i].owrite == ENABLE){
						pio->node->PIO_OWER |= (0x01u << i);
					}
					else
					{
						pio->node->PIO_OWDR |= (0x01u << i);
					}
					pio->node->PIO_PER |= (0x01u << i);
					if(pio->list[i].pin_state == ON){
						pio->node->PIO_SODR |= (0x01u << i);
					}
					else
					{
						pio->node->PIO_SODR &= ~(0x01u << i);
					}	
				}
				if(pio->list[i].multidriver == ENABLE){
					pio->node->PIO_MDER |= (0x01u << i);
				}
				else
				{
					pio->node->PIO_MDDR |= (0x01u << i);					
				}
				if(pio->list[i].interrupt == ENABLE){
					pio->node->PIO_IER |= (0x01u << i);					
				}
				
			}
			else
			{
				pio->node->PIO_PDR |= (0x01u << i);
				switch(pio->list[i].periph){
					case PERIPHA:{
						pio->node->PIO_ABCDSR[0] &= ~( 0x01u << i);
						pio->node->PIO_ABCDSR[1] &= ~( 0x01u << i);
						break;
					}
					case PERIPHB:{
						pio->node->PIO_ABCDSR[0] |= ( 0x01u << i);
						pio->node->PIO_ABCDSR[1] &= ~( 0x01u << i);
						break;
					}
					case PERIPHC:{
						pio->node->PIO_ABCDSR[0] &= ~( 0x01u << i);
						pio->node->PIO_ABCDSR[1] |= ( 0x01u << i);
						break;
					}
					case PERHIPD:{
						pio->node->PIO_ABCDSR[0] |= ( 0x01u << i);
						pio->node->PIO_ABCDSR[1] |= ( 0x01u << i);
						break;
					}
					default:{
						pio->node->PIO_ABCDSR[0] &= ~( 0x01u << i);
						pio->node->PIO_ABCDSR[1] &= ~( 0x01u << i);
						break;
						
						}
				}
			}
		}
		pio->list[i].b_registered = bTRUE;
	}
}

void pio_deinit(struct pio *pio){

}

 void pio_change(
		 pio *pio,
		 uint8_t pin_num,
		char *name,
		uint8_t pio_endis,
		uint8_t io,
		uint8_t pupd,
		uint8_t multidriver,
		uint8_t owrite,
		uint8_t interrupt,
		uint8_t pin_state,
		uint8_t periph,
		uint8_t i_edge_sel,
		uint8_t i_rehl_sel,
		uint8_t i_add_irq_mode
		)
 {
	 /* TODO:: implement safety check if pio is already initialized*/
	 if (pio != 0x00U){
		 pio->list[pin_num].name = name;
		 pio->list[pin_num].pio_endis = pio_endis;
		 pio->list[pin_num].io = io;
		 pio->list[pin_num].pupd = pupd;
		 pio->list[pin_num].multidriver = multidriver;
		 pio->list[pin_num].owrite = owrite;
		 pio->list[pin_num].interrupt = interrupt;
		 pio->list[pin_num].pin_state = pin_state;
		 pio->list[pin_num].periph = periph;
		 pio->list[pin_num].i_edge_sel = i_edge_sel;
		 pio->list[pin_num].i_rehl_sel = i_rehl_sel;
		 pio->list[pin_num].i_add_irq_mode = i_add_irq_mode;
		 pio->list[pin_num].b_registered = bTRUE;
	 }
	 pio_init(pio);
 }
 
 void pio_switch(struct pio *pself, uint8_t pin, uint8_t u8switch){
	 if(u8switch == ON){
		pself->node->PIO_SODR	|= (0x01 << pin);
	 }
	 else
	 {
		 pself->node->PIO_CODR |= (0x01 << pin);
	 }	 
 }
 
 void pio_pioa_on_irq(uint32_t stat_val){
	 /* TODO:: this must be a registered callback functions to achive sw architecture*/
	 /* TODO:: pio irq handle*/

 }
 
void pio_pioc_on_irq(uint32_t stat_val){
	/* TODO:: this must be a registered callback functions to achive sw architecture*/
	if(stat_val & (0x1u << 19)){
		if_switch_on_switch1();
	}
	if(stat_val & (0x1u << 20)){
		if_switch_on_switch2();
	}
	if(stat_val & (0x1u << 21)){
		if_switch_on_switch3();
	}	
	if(stat_val & (0x1u << 22)){
		if_switch_on_switch4();
	}
}

void pio_toggle_led(uint8_t led_mask){
	if(PIOC->PIO_PDSR & led_mask){
		PIOC->PIO_CODR |= led_mask;
	}
	else
	{
		PIOC->PIO_SODR |= led_mask;
	}
}

