/*
 * pmc.c
 *
 * Created: 23.06.2018 12:29:00
 *  Author: vB1w
 */ 

#include "drv/atsame70q20/pmc.h"

 pll_clock s_plla = {
 		.enable = bTRUE,
 		.speed_set = PMC_PLLA_160M,
 		.speed_get = 0x00U,
 		.speed_tolerance = 1000U,
 		.speed_max = PMC_PLLA_MAX,
 		.division = 0x00U,
 		.multiplication = 0x00U
 };
 
 crystal_clock s_crystal = {
	.is_init = bFALSE,
	.enable = bFALSE,
 	.speed_set = PMC_EXT_OSZILLATOR_16M,
 	.speed_get = 0U,
 	.speed_tolerance = PMC_EXT_OSZILLATOR_TOLERANCE,
 	.speed_max = PMC_EXT_OSZILLATOR_16M_MAX,
 	.startup_time = PMC_CRYSTAL_STARTUP_TIME_MAX
 };

 struct programable_clock arr_prog[PERIPHERAL_CLOCK_MAX];
 struct peripheral_clock arr_periph[PROGRAMABLE_CLOCK_MAX];
 
 struct main_clock s_main = {
	.is_init = NO,
	.source = PMC_INTERNAL_OSZILLATOR,
	.speed_set = PMC_MAIN_RC_4M,	 
	.speed_get = 0U,
	.speed_tolerance = PMC_EXT_OSZILLATOR_TOLERANCE,
	.speed_ok = NO,
	.speed_max = (PMC_MAIN_RC_12M + PMC_EXT_OSZILLATOR_TOLERANCE)
};

 struct master_clock s_master = {
	.is_init = NO
};

pmc s_pmc = {
	.name = "PMC",
	.node = ((Pmc	*)0x400E0600U),
	.meas_speed = 0U,
	.oszillator_name = 0x00U,
 	.pcrystal_clk = &s_crystal,
 	.pmaster_clk = &s_master,
 	.pprog_clk = arr_prog,
 	.pperiph_clk = arr_periph,
 	.pmain_clk = &s_main,
 	.ppllx_clk = &s_plla
};

void pmc_init(pmc *pself){
	
	pself->is_init = bFALSE;
	pself->pmain_clk->is_init = bFALSE;
	pself->ppllx_clk-> is_init = bFALSE;
	
	/*activat supplies for crystals through voltage regualtors in SUPC*/
	SUPC->SUPC_CR = SUPC_CR_VROFF_NO_EFFECT | SUPC_CR_XTALSEL_CRYSTAL_SEL | SUPC_CR_KEY_PASSWD;
	SUPC->SUPC_MR = SUPC_MR_ONREG_ONREG_USED | SUPC_MR_KEY_PASSWD;
	
	EFC->EEFC_FMR = EEFC_FMR_FWS(5);	// found out according to sw example; cant say what this does; section 31.14 datasheet

	pmc_main_oscillator_select(pself, PMC_EXTERNAL_OSZILLATOR);
	pmc_main_frequency_select(pself, PMC_EXT_OSZILLATOR_16M);
	pmc_plla_enable(pself, PMC_PLL_DISABLE);
	
}

void pmc_run(pmc *pself){
	
	/*do initialization if not yet established*/
	if(pself->is_init == bFALSE){
		if(pself->pmain_clk->is_init == bFALSE){
			
			pmc_main_clk_init(pself);
			pmc_main_clk_check(pself);
			
			}
			else if (
			(pself->pmain_clk->is_init == bTRUE) &&
			(pself->ppllx_clk->is_init == bFALSE) &&
			(pself->ppllx_clk->enable == bTRUE)
			){
				pmc_plla_init(pself);
				pmc_plla_check(pself);
			}
			else
			{
				pself->is_init = YES;
			}
	}
	/*do check routines when enabled*/
	else
	{
		/*MAIN CLK*/
		pmc_main_clk_check(pself);
		if(pself->pmain_clk->speed_ok == bFALSE){
			pself->pmain_clk->is_init = bFALSE;
			/*try to init again*/
		}
		/*PLLA CLK*/
		else if((pself->ppllx_clk->speed_ok == bFALSE) && (pself->ppllx_clk->enable == bTRUE)){
			pself->ppllx_clk->is_init = bFALSE;
			/*try to init again*/
		}
	}
}

void pmc_main_clk_init(pmc *pself){

	if(pself->pmain_clk->source == PMC_INTERNAL_OSZILLATOR){
		pmc_rc_init(pself);
		pmc_rc_check(pself);
	}
	else if(pself->pmain_clk->source == PMC_EXTERNAL_OSZILLATOR){
		
		pmc_crystal_init(pself);
		pmc_crystal_check(pself);
	}
	// wait for external or internal crystal oscillator is completely established
	/* TODO:: check over nvic later*/
	while(!(PMC->PMC_SR & PMC_SR_MOSCSELS));
	pself->pmain_clk->is_init = bTRUE;
}

void pmc_rc_init(pmc *pself){
	
	PMC->CKGR_MOR = CKGR_MOR_KEY_PASSWD | CKGR_MOR_MOSCRCEN;
	
	switch(pself->pmain_clk->speed_set){
		case PMC_MAIN_RC_4M: {
			PMC->CKGR_MOR |= CKGR_MOR_KEY_PASSWD | CKGR_MOR_MOSCRCF_4_MHz;
			break;
		}
		case PMC_MAIN_RC_8M: {
			PMC->CKGR_MOR |= CKGR_MOR_KEY_PASSWD | CKGR_MOR_MOSCRCF_8_MHz;
			break;
		}
		case PMC_MAIN_RC_12M: {
			PMC->CKGR_MOR |= CKGR_MOR_KEY_PASSWD | CKGR_MOR_MOSCRCF_12_MHz;
			break;
		}
		default: {
			/* exit with error message*/
			break;
		}
	}
}

void pmc_rc_check(pmc *pself){
	
}

void pmc_crystal_init(pmc *pself){
	
	uint16_t u16startup_time = pself->pcrystal_clk->startup_time;

	/// INFO in this case (strawberry) external crystal is 16MHz
	PMC->CKGR_MOR |= CKGR_MOR_MOSCXTEN| (0x0u << 1) | CKGR_MOR_MOSCXTST(u16startup_time) | CKGR_MOR_KEY_PASSWD;
	// wait for stabilized external crystal oscillator
	while(!(PMC->PMC_SR & PMC_SR_MOSCXTS));
	
	PMC->CKGR_MOR |= CKGR_MOR_MOSCSEL| CKGR_MOR_KEY_PASSWD;
	pself->pcrystal_clk->is_init = bTRUE;
}

void pmc_crystal_check(struct pmc *pself){
	
}


void pmc_main_oscillator_select(pmc *pself, uint8_t oszillator_name){
	if (oszillator_name == PMC_EXTERNAL_OSZILLATOR){
		pself->pcrystal_clk->enable = bTRUE;
	}
	pself->pmain_clk->source = oszillator_name;
}

void pmc_main_frequency_select(pmc *pself, uint32_t frq){
	pself->pmain_clk->speed_set = frq;
}

void pmc_plla_enable(struct pmc *pself, uint8_t u8endis){
	pself->ppllx_clk->enable = u8endis;	
}


void pmc_master_clk_calc(pmc *pself){
/*calculate if there is a div or mul to get desired cpu main clock frequency*/	

}

void pmc_main_clk_check(pmc *pself){
	
	uint32_t frequency = 0x0000U;	
	uint16_t frq_ticks_slow_clock = 0x0000U;
	
	if(pself->pmain_clk->source == PMC_EXTERNAL_OSZILLATOR){
		PMC->CKGR_MCFR |= CKGR_MCFR_RCMEAS | CKGR_MCFR_CCSS;	
	}
	else if(pself->pmain_clk->source == PMC_INTERNAL_OSZILLATOR){
		PMC->CKGR_MCFR |= CKGR_MCFR_RCMEAS;
	}

	/*wait till measurement is ready*/
	/* TODO:: do check over nvic*/
	while(!(PMC->CKGR_MCFR & CKGR_MCFR_MAINFRDY)){
		uint32_t temp = PMC->CKGR_MCFR;		/// INFO:: read once before proceeding (see datasheet)
	}
			
	frq_ticks_slow_clock = (PMC->CKGR_MCFR & CKGR_MCFR_MAINF_Msk);

	frequency = frq_ticks_slow_clock * PMC_SLOW_CLK_32K / 16;
	pself->pmain_clk->speed_get = frequency;

	
	if ((pself->pmain_clk->speed_get < (pself->pmain_clk->speed_set - pself->pmain_clk->speed_tolerance))
	|| (pself->pmain_clk->speed_get > (pself->pmain_clk->speed_set + pself->pmain_clk->speed_tolerance)))
	{
		pself->pmain_clk->speed_ok = bFALSE;
	}
	else
	{
		pself->pmain_clk->speed_ok = bTRUE;
	}
}


void pmc_plla_init(pmc *pself){
	pmc_plla_calc(pself); /* get div and mul trough calc in this function to get required pll frequency*/
	
	// enable plla through diva and mulitply with mula
	PMC->CKGR_PLLAR |= CKGR_PLLAR_DIVA(pself->ppllx_clk->division) | CKGR_PLLAR_MULA(pself->ppllx_clk->multiplication) | CKGR_PLLAR_ONE;
	while(!(PMC->PMC_SR & PMC_SR_LOCKA));
	PMC->PMC_MCKR = PMC_MCKR_CSS_PLLA_CLK;
	while(!(PMC->PMC_SR & PMC_SR_MCKRDY));
	
}

void pmc_plla_check(pmc *pself){
	/* TODO:: check out in datasheet how to check pll clock*/
	if (pself->ppllx_clk->enable == bTRUE && pself->ppllx_clk->is_init){
		/* TODO:: get all available plla checks*/
		
		/* if check failes because of wrong values deinit plla*/
	}
}

void pmc_plla_calc(pmc *pself){
	// TODO:: calculates the div and mul for specified clock speed, currently fixed values
	
	uint32_t freq_target  = pself->ppllx_clk->speed_set;
	uint32_t freq_tolerance = pself->ppllx_clk->speed_tolerance;
	uint32_t freq_approx = 0U;
	uint8_t div = 1U;
	uint8_t mul = 0U;
	
	mul = freq_target/pself->ppllx_clk->speed_set;
	pself->ppllx_clk->division = div;
	pself->ppllx_clk->multiplication = mul;	
}

void pmc_init_peripheral_clk(uint8_t pmc_pid, uint8_t pmc_clk_sel){
	
	/* init pmc --> peripheral clock*/
	PMC->PMC_PCR |= (pmc_pid << PMC_PCR_PID_Pos)| (pmc_clk_sel << 8)| PMC_PCR_CMD| PMC_PCR_EN| (0x1u << 29);
}

void select_internal_oscillator_12mhz(void){
	s_pmc.pmain_clk->source = PMC_INTERNAL_OSZILLATOR;
}

void select_external_oscillator_crystal(void){
	s_pmc.pmain_clk->source = PMC_EXTERNAL_OSZILLATOR;
}

uint32_t get_main_oscillator_frequency(void){
	uint32_t u32ret = 0x00U;
	if(s_pmc.pmain_clk->is_init == bTRUE){
		u32ret = s_pmc.pmain_clk->speed_get;
	}
	return u32ret;
}

uint8_t is_main_oscillator_ok(void){
	
	if(s_pmc.pmain_clk->speed_ok == bTRUE){
		return bTRUE;
	}
	else{
		return bFALSE;
	}
}