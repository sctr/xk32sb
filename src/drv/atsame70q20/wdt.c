/*
 * sb_wdt.c
 *
 * Created: 01.10.2018 19:17:54
 *  Author: vB1w
 */ 

#include "sam.h"
#include "drv/atsame70q20/wdt.h"

void sb_wdt_init(uint32_t sb_wdt_time){
	WDT->WDT_MR |= WDT_MR_WDV(sb_wdt_time) | WDT_MR_WDDBGHLT;
}

void sb_wdt_rst(void){
	WDT->WDT_CR |= WDT_CR_WDRSTT | WDT_CR_KEY_PASSWD;
}