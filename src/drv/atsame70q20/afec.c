/*
 * afec.c
 *
 * Created: 14.10.2018 15:30:29
 *  Author: vB1w
 */ 

#include "drv/atsame70q20/afec.h"

char digit[10] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

/* struct names must be four ascii signs long*/
/* struct newval is for update of the message routine*/
/*
	|available|
			|newval|
				|value|
						|valascii|
								|name_channel|
											|name_function|
														|offset|
*/
afec_ch_list s_afec0_ch[CH_TOT] = {
	{ YES,	NO,	 0x00u,	 "----", "CH0",		 "NTC0",	0x200u	},
	{ NO,	NO,	 0x00u,	 "----", "CH1",		 "n/a",		0x00u	},
	{ NO,	NO,	 0x00u,	 "----", "CH2",		 "NTC2",	0x200u	},
	{ NO,	NO,	 0x00u,	 "----", "CH3",		 "NTC3",	0x200u	},
	{ NO,	NO,	 0x00u,	 "----", "CH4",		 "NTC4",	0x200u	},
	{ YES,	NO,	 0x00u,	 "----", "CH5",		 "NTC5",	0x200u	},
	{ NO,	NO,	 0x00u,	 "----", "CH6",		 "NTC6",	0x200u	},
	{ YES,	NO,	 0x00u,	 "----", "CH7",		 "Poti",	0x200u	},
	{ NO,	NO,	 0x00u,	 "----", "CH8",		 "NTC7",	0x200u	},
	{ NO,	NO,	 0x00u,	 "----", "CH9",		 "NTC1",	0x200u	},
	{ NO,	NO,	 0x00u,	 "----", "CH10",	 "n/a",		0x00u	},
	{ YES,	NO,	 0x00u,	 "----", "CH11",	 "uCTp",	0x200u	}
};

afec_ch_list s_afec1_ch[CH_TOT] = {
	{ NO,	NO,	 0x00u,	 "----", "CH0",		 "n/a",		0x00u	},
	{ NO,	NO,	 0x00u,	 "----", "CH1",		 "n/a",		0x00u	},
	{ NO,	NO,	 0x00u,	 "----", "CH2",		 "n/a",		0x00u	},
	{ NO,	NO,	 0x00u,	 "----", "CH3",		 "n/a",		0x00u	},
	{ YES,	NO,	 0x00u,	 "----", "CH4",		 "V__U",	0x200u	},
	{ YES,	NO,	 0x00u,	 "----", "CH5",		 "V__V",	0x200u	},
	{ YES,	NO,	 0x00u,	 "----", "CH6",		 "V__W",	0x200u	},
	{ NO,	NO,	 0x00u,	 "----", "CH7",		 "n/a",		0x00u	},
	{ NO,	NO,	 0x00u,	 "----", "CH8",		 "n/a",		0x00u	},
	{ NO,	NO,	 0x00u,	 "----", "CH9",		 "n/a",		0x00u	},
	{ NO,	NO,	 0x00u,	 "----", "CH10",	 "n/a",		0x00u	},
	{ NO,	NO,	 0x00u,	 "----", "CH11",	 "n/a",		0x00u	}
};

afec s_afec0 = {
	"AFEC0",
	((Afec   *)0x4003C000U),
	0x03,
	AFEC_MR_STARTUP_SUT512,
	s_afec0_ch
	};
	
afec s_afec1 = {
	"AFEC1",
	((Afec   *)0x40064000U),
	0x03,
	AFEC_MR_STARTUP_SUT512,
	s_afec1_ch
	};

void afec_init(afec *pself){
	pself->node->AFEC_MR |= AFEC_MR_ONE | pself->startuptime | AFEC_MR_PRESCAL(pself->prescale);
	for(uint8_t i = 0; i < CH_TOT; i++){
		if(pself->ch_list[i].available == YES){
			pself->node->AFEC_CHER |= (0x01u << i);
			afec_aoff(pself, i, 0x200u);
			pself->node->AFEC_IER |= (0x01u << i);
		}
	}
	pself->node->AFEC_ACR |= AFEC_ACR_PGA0EN | AFEC_ACR_PGA1EN;
	pself->u8_term_out_wait = 0x00U;
}

void afec_run(afec *pself){

	if(pself->u8_term_out_wait >= 0x40U){
		xk32_term_out_msg("\e[2J");
		for(uint8_t u8ch = 0x00U; u8ch < CH_TOT; u8ch++){
			if(pself->ch_list[u8ch].newval == YES){
				xk32_term_out_msg(pself->ch_list[u8ch].name_function);
				xk32_term_out_msg(pself->ch_list[u8ch].value_ascii);
				pself->ch_list[u8ch].newval = NO;
			}
		}
		pself->u8_term_out_wait = 0x00U;
	}
	pself->u8_term_out_wait++;
}

void afec_aoff(afec *pself, uint16_t ch, uint16_t offset){
	pself->node->AFEC_CSELR = ch;
	pself->node->AFEC_COCR = offset;
}

void afec_start_conversion(afec *pself){
	pself->node->AFEC_CR |= AFEC_CR_START;
}

void afec_on_irq(afec *pself, uint32_t irq_val){
	for(uint8_t u8ch = 0x00U; u8ch < CH_TOT; u8ch++){
		if(pself->ch_list[u8ch].available ==YES){
			if(irq_val & (0x01 << u8ch)){
				afec_store_ch_val(pself, u8ch);				
				afec_prepare4term_out(pself, u8ch);
			}
		}
	}
}

void afec_store_ch_val(afec *pself, uint8_t ch){
	pself->node->AFEC_CSELR =ch;
	pself->ch_list[ch].value = pself->node->AFEC_CDR;
}

void afec_prepare4term_out(afec *pself, uint8_t ch){
	afec_itoa(pself, ch); // switch to ascii for uart/ terminal
	afec_newval(pself, ch);
}

void afec_itoa(afec *pself, uint8_t ch){
	uint32_t num = pself->ch_list[ch].value;
	uint16_t divider = 1000;
	uint32_t tmp = 0;
	uint32_t substract = 0;
	for(uint8_t i = 0; i < 4; i++){
		tmp = num/divider;
		substract = tmp * divider;
		pself->ch_list[ch].value_ascii[i] = digit[tmp];
		num = num - substract;
		divider = divider/ 10;
	}
}

void afec_newval(afec *pself, uint8_t ch){
	pself->ch_list[ch].newval = YES;
}