/*
 * uart.c
 *
 * Created: 21.06.2018 22:07:48
 *  Author: vB1w
 */ 

#include "drv/atsame70q20/uart.h"

#define UART_WAIT	10000


uart s_uart0 = {
	.name = "UART0",
	.node = ((Uart *)0x400E0800U),
	.is_init = bFALSE,
	.baudrate = 0x00U,
	.parity_bit = 0x00U,
	.clock_divisor = 0x00U,
	.main_freq = 0x00U,
	.main_freq_is_init = bFALSE,
	.irq_stat_bits = 0x00U
};

uart s_uart1 = {
	.name = "UART1",
	.node = ((Uart *)0x400E0A00U),
	.is_init = bFALSE,
	.baudrate = 0x00U,
	.parity_bit = 0x00U,
	.clock_divisor = 0x00U,
	.main_freq = 0x00U,
	.main_freq_is_init = bFALSE,
	.irq_stat_bits = 0x00U
};

void uart_0_init(uint32_t baud, uint8_t parity){
	uart_init(&s_uart0, baud, parity);
}


void uart_1_init(uint32_t baud, uint8_t parity){
	uart_init(&s_uart1, baud, parity);
}

void uart_init(uart *pself, uint32_t baud, uint8_t parity){

	pself->main_freq = get_main_oscillator_frequency();
	pself->main_freq_is_init = is_main_oscillator_ok();
	if(pself->main_freq_is_init == bTRUE){
		pself->baudrate = baud;
		pself->parity_bit = parity;
		pself->node->UART_CR |= UART_CR_RSTTX;
		pself->node->UART_CR |= UART_CR_RSTSTA;
		/* TODO:: approximate calculation*/
		/* 921600*/
		pself->clock_divisor = pself->main_freq/(16*pself->baudrate);			/// INFO:: calculation from datasheet
		pself->node->UART_BRGR = 1U;//pself->clock_divisor; // Uart set baud rate scaler
		pself->node->UART_MR |= (parity << 9);
		/*UART0->UART_CR |= UART_CR_REQCLR;*/
		pself->node->UART_IER |= UART_IER_TXEMPTY/* | UART_IER_TXRDY*/;
		pself->node->UART_CR |= (0x1u << 6); // Uart transmitter enable
		pself->node->UART_CR |= (0x1u << 4); // Uart receiver enable
		pself->is_init = bTRUE;
	}
	else{
		pself->is_init = bFALSE;
	}
}

uint8_t uart_0_check(void){
	return uart_check(&s_uart0);	
}

uint8_t uart_1_check(void){
	return uart_check(&s_uart0);
}

uint8_t uart_check(uart *pself){

	pself->main_freq_is_init = is_main_oscillator_ok();
	
	if(pself->main_freq_is_init == bTRUE)
		pself->main_freq = get_main_oscillator_frequency();
	else
		pself->is_init = bFALSE;
	
	/* TODO:: other checks available?*/
	
	return bTRUE;
}

uint8_t uart_is_init(void){
	if(s_uart0.is_init == bTRUE){
		return bTRUE;
	}
	else{
		return bFALSE;
	}
}

void uart_set_transmit_val(char trans_val){
		UART0->UART_THR = trans_val;
}

char uart_get_received_val(){
	return UART0->UART_RHR;
}

/*	TODO:: manage interrupt bits:
	- read one time
	- analyse which status bits was set
	
*/
void uart_get_irq_stat_bits(uart *pself){
	
	pself->irq_stat_bits = pself->node->UART_IMR; /* TODO:: get uart0 node*/
	
}

uint32_t uart_0_tx_empty_irq(void){

	return uart_tx_empty_irq(&s_uart0);
	
}

uint32_t uart_1_tx_empty_irq(void){

	return uart_tx_empty_irq(&s_uart1);
	
}

uint32_t uart_tx_empty_irq(uart *pself){
	
	uint32_t ret = pself->node->UART_IMR & UART_IMR_TXEMPTY;
	if(ret == bTRUE){
		pself->irq_stat_bits &= ~ (0x00U < UART_IMR_TXEMPTY); /*clear bit for next interrupt status readout*/
	}
	return (ret);
}

uint32_t uart_0_rx_irq(void){
	return uart_rx_irq(&s_uart0);
}

uint32_t uart_1_rx_irq(void){
	return uart_rx_irq(&s_uart1);
}

uint32_t uart_rx_irq(uart *pself){
	return((pself->node->UART_IMR & UART_IMR_RXRDY));
}
