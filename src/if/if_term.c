/*
 * if_term.c
 *
 * Created: 10.02.2021 18:37:26
 *  Author: vB1w
 */ 

#include "if/if_term.h"

void if_term_init(if_term *pself){
	xk32_term_init(&s_xk32_term, pself->baudrate, pself->parity);
}

void if_term_run(if_term *pself){
	xk32_term_run(&s_xk32_term);
}