/*
 * if_led.c
 *
 * Created: 27.07.2019 01:47:29
 *  Author: vB1w
 */ 

#include "if/if_led.h"

struct if_led_dedicated_led s_if_led0 = {		
		bTRUE,
		900,
		100,
		0x00u,
		0x00u,
		bFALSE,		
		OFF,
		"LED0",
		PORTC,
		0U
};

struct if_led_dedicated_led s_led1 = {		
		bTRUE,
		900,
		100,
		0x00u,
		0x00u,
		bFALSE,		
		OFF,
		"LED1",
		PORTC,
		1U
};

struct if_led_dedicated_led s_if_led2 = {		
		bTRUE,
		950,
		100,
		0x00u,
		0x00u,
		bFALSE,		
		OFF,
		"LED2",
		PORTC,
		2U
};

struct if_led_dedicated_led s_led3 = {		
		bTRUE,
		900,
		100,
		0x00u,
		0x00u,
		bFALSE,		
		OFF,
		"LED3",
		PORTC,
		3U
};

void if_led_init(void){
	
	if_led_register(&s_if_led0);
	//led_register(&s_led1);
	if_led_register(&s_if_led2);
	//led_register(&s_led3);
}

void if_led_register(if_led_dedicated_led *pself){
	xk32_gpio_register_led(pself->port, pself->pin, pself->name);
	/* invoke timer*/
	xk32_timer_start(&s_timer, pself->toggle_time_on, if_led_on_irq, pself);
}

/*1ms task enough for update?*/
void if_led_update(void){
	if_led_run(&s_if_led0);
	//led_run(&s_led1);
	if_led_run(&s_if_led2);
	//led_run(&s_led3);
}

void if_led_run(if_led_dedicated_led *pself){
	//pself->ctr_update++;
	if(pself->change == bTRUE){
		pio_switch(&s_pioc, pself->pin, pself->state);
		if(pself->toggling == bTRUE){
			if(pself->state == ON){
				xk32_timer_start(&s_timer, pself->toggle_time_on, if_led_on_irq, pself);
				pself->state = OFF;
			}
			else{
				xk32_timer_start(&s_timer, pself->toggle_time_off, if_led_on_irq, pself);
				pself->state = ON;
			}
		}
		pself->change = bFALSE;
	}
}

void if_led_set(if_led_dedicated_led *pself, uint8_t new_state){
	if(pself->toggling == bFALSE){
		pself->state = new_state;
	}
	pself->change = bTRUE;
	//pself->ctr_irq++;
}

void if_led_on_irq(void *pself){
	if_led_set(pself, ON);
}

void if_led0_on(void){
	
}

void if_led0_off(void){
	
}