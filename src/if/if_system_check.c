/*
 * if_system_check.c
 *
 * Created: 25.01.2021 13:52:33
 *  Author: vB1w
 */ 

#include "if/if_system_check.h"

void if_sys_check_init(struct if_system_check *pself){
	
}

void if_sys_check_run(struct if_system_check *pself){
	// reset watchdog timer
	sb_wdt_rst();	/* TODO:: consider architecture*/
	// TODO:: Power manager also here, or seperate task?
	
	return 0u;
}