/*
 * if_pm.c
 *
 * Created: 22.01.2021 09:47:45
 *  Author: vB1w
 */ 

#include "if/if_pm.h"

struct xk32_clock s_clock = {
	.is_initialized = bFALSE,
	.is_stable = bFALSE
};

void if_pm_init(if_pm *pself){
	/* TODO:: invoke xk32_clock -> invoke drv/pmc */
	
	xk32_clock_init(&s_clock);
	xk32_clock_init_peripheral(&s_clock);
	
	/* TODO:: establish usb power management*/

	pself->is_initialized = bTRUE;
}

void if_pm_run(if_pm *pself){
	/* ? here initialization check (is_initialized)*/
	/* task for checking if all clocks are good*/
// 	if(pself->is_initialized == bTRUE){
// 		sb_pm_check(pself);
// 	}
	xk32_clock_run(&s_clock);
	
}

void if_pm_check(if_pm *pself){
	/* check clock speed */
	/* check usb clock*/
	
}