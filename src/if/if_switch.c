/*
 * if_switch.c
 *
 * Created: 13.10.2018 20:49:55
 *  Author: vB1w
 */ 

#include "if/if_switch.h"

struct if_switch s_sw0 = {
	.name = "Button_LowSide",
	.description = "switches low side Mosfets -> charging bootstrap cap",
	.is_init = bFALSE,
	.is_trig = bFALSE
};

struct if_switch s_sw1 = {
	.name = "Button_ManualCommutation",
	.description = "manual step by step commutation",
	.is_init = bFALSE,
	.is_trig = bFALSE
};

struct if_switch s_sw2 = {
	.name = "SWCH2",
	.description = "\0",
	.is_init = bFALSE,
	.is_trig = bFALSE
};

struct if_switch s_sw3 = {
	.name = "SWCH3",
	.description = "\0",
	.is_init = bFALSE,
	.is_trig = bFALSE
};

void if_switch_init(if_switch *pself){
	
}

void if_switch_run(if_switch *pself, uint8_t is_on){
	
}

/*TODO:: generalize "on_switch()" and capture all switches -- with callback functions*/
void if_switch_on_switch1(void){
	xk32_term_out_msg("Pushed switch 1 -> nothing implemented\n");
}

void if_switch_on_switch2(void){
	xk32_term_out_msg("Pushed switch 2 -> nothing implemented\n");	
}

void if_switch_on_switch3(void){
	/* TODO:: uncomment for motor driver development*/
	sb_mot_ctrl_cmd_load_cap();
}

void if_switch_on_switch4(void){
	/* TODO:: uncomment for motor driver development*/
	sb_mot_ctrl_cmd_start();
}
