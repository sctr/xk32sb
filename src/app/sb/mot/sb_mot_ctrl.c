/*
 * sb_mot_ctrl.c
 *
 * Created: 16.11.2018 19:37:57
 *  Author: vB1w
 */ 

#include "app/sb/mot/sb_mot_ctrl.h"

struct sb_mot_ctrl s_sb_mot_ctrl = {
		.e_sb_mot_ctrl_comu = comu_step_manual,
		.is_init = 0x00U,
		.init_ctr = 0x00U,
		.init_ctr_max = 0x10U,
		.cmd_load_cap = bFALSE,
		.cmd_start = bFALSE
};

void sb_mot_ctrl_init(sb_mot_ctrl *pself){
	switch(pself->e_sb_mot_ctrl_comu){
		case comu_step_manual: {
			sb_comu_step_man_init(&s_sb_comu_step_man);
			pself->is_init = bTRUE;
			break;
		}
		case comu_step: {
			/*invoke step commutation variant*/
			break;
		}
		case comu_block:{
			/*invoke block commutation variant*/
			break;
		}
		case comu_foc:{
			/*invoke foc variant*/
			break;
		}
		default: break;
	}
}

void sb_mot_ctrl_update(sb_mot_ctrl *pself){
	/* check if mot initialized*/
	if(pself->is_init == bTRUE){
		sb_comu_step_man_update(&s_sb_comu_step_man);
	}
	else
	{
		/* init again? and count up!*/
	}
	/* check commands*/
	if(pself->cmd_load_cap == bTRUE){
		sb_comu_step_man_load_cap();
		pself->cmd_load_cap = bFALSE;
	}
	else if(pself->cmd_start == bTRUE){
		sb_comu_step_man_start();
		pself->cmd_start = bFALSE;
	}
}

void sb_mot_ctrl_cmd_load_cap(void){
	s_sb_mot_ctrl.cmd_load_cap = bTRUE;
}

void sb_mot_ctrl_cmd_start(void){
	s_sb_mot_ctrl.cmd_start = bTRUE;
}

void sb_mot_ctrl_cmd_stop(void){
	s_sb_mot_ctrl.cmd_start = bFALSE;
}