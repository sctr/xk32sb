/*
 * sb_comu_step_man.c
 *
 * Created: 24.07.2019 22:38:50
 *  Author: vB1w
 */ 

#include "app/sb/mot/commutation/sb_comu_step_man.h"

struct sb_comu_step_man s_sb_comu_step_man = {
		.time_load_cap = 100U,
		.time_commutation = 50U,
		.s_sb_comu_step_man_table = &s_sb_comu_step_man_table[0x00U],
		.table_ctrl_ctr = 0x00
};

void sb_comu_step_man_init(sb_comu_step_man *s_step_man){
	sb_pwm_init(&s_sb_pwm0); /* TODO:: change to a general init*/
}

void sb_comu_step_man_update(sb_comu_step_man *s_step_man){
	/* check current*/
}

void sb_comu_step_man_start(void){
	sb_comu_step_man_next_phase(&s_sb_comu_step_man);
}

void sb_comu_step_man_load_cap(void){
	sb_comu_step_man_low_side_all(&s_sb_comu_step_man);
}

void sb_comu_step_man_next_phase(sb_comu_step_man *pself){
	//turn phase on due to struct
	sb_comu_step_man_turn_off();
	/* switch low side pwm due to table*/
	if(pself->s_sb_comu_step_man_table->phaseUL == PHASE_HIGH){
		PWM0->PWM_ENA |= PWM_ENA_CHID1;
	}
	else
	{
		PWM0->PWM_DIS |= PWM_DIS_CHID1;
	}
	if(pself->s_sb_comu_step_man_table->phaseVL == PHASE_HIGH){
		PWM1->PWM_ENA |= PWM_ENA_CHID1;
	}
	else
	{
		PWM1->PWM_DIS |= PWM_DIS_CHID1;
	}
	if(pself->s_sb_comu_step_man_table->phaseWL == PHASE_HIGH){
		PWM1->PWM_ENA |= PWM_ENA_CHID3;
	}
	else
	{
		PWM1->PWM_DIS |= PWM_DIS_CHID3;
	}
	/*switch high side mosfets due to table -> permanently on till timer is over*/
	pio_switch(&s_pioa, 0x00u, (uint8_t) pself->s_sb_comu_step_man_table->phaseUH);
	pio_switch(&s_piod, 0x01u, (uint8_t) pself->s_sb_comu_step_man_table->phaseVH);
	pio_switch(&s_piod, 0x05u, (uint8_t) pself->s_sb_comu_step_man_table->phaseWH);
	
	pself->table_ctrl_ctr++;
	if(pself->table_ctrl_ctr >= 0x03u){
		pself->table_ctrl_ctr = 0x00u;
	}
	/* safety feature-> double invocation*/
	if(pself->table_ctrl_ctr >= 0x03u){
		pself->table_ctrl_ctr = 0x00u;
	}
	pself->s_sb_comu_step_man_table = &s_sb_comu_step_man_table[pself->table_ctrl_ctr];

	/*run timer with callback invocation */
	xk32_timer_start(&s_timer, pself->time_commutation, sb_comu_step_man_turn_off, 0x00U);
}

void sb_comu_step_man_turn_off(void){
	PWM0->PWM_DIS |= PWM_ENA_CHID1;	//PA2 0PWMH1
	PWM1->PWM_DIS |= PWM_ENA_CHID1| PWM_ENA_CHID3; //PD3 1PWMH1 | PD7 1PWMH3
	pio_switch(&s_pioa, 0x00u, OFF);
	pio_switch(&s_piod, 0x01u, OFF);
	pio_switch(&s_piod, 0x05u, OFF);
}

void sb_comu_step_man_low_side_all(sb_comu_step_man *pself){
	PWM0->PWM_ENA |= PWM_ENA_CHID1;	//PA2 0PWMH1
	PWM1->PWM_ENA |= PWM_ENA_CHID1| PWM_ENA_CHID3; //PD3 1PWMH1 | PD7 1PWMH3

	/*run timer with callback invokation */
	xk32_timer_start(&s_timer, pself->time_load_cap, sb_comu_step_man_turn_off, 0x00U);
}