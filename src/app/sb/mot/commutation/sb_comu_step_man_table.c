/*
 * sb_comu_step_man_table.c
 *
 * Created: 22.10.2018 17:10:52
 *  Author: vB1w
 */ 

#include "app/sb/mot/commutation/sb_comu_step_man_table.h"

struct sb_comu_step_man_table s_sb_comu_step_man_table[SB_MOT_PHASE] = {
	{PHASE_HIGH, PHASE_LOW, PHASE_LOW, PHASE_HIGH, PHASE_HIGH, PHASE_LOW},
	{PHASE_LOW, PHASE_HIGH, PHASE_HIGH, PHASE_LOW, PHASE_HIGH, PHASE_LOW},
	{PHASE_HIGH, PHASE_LOW, PHASE_HIGH, PHASE_LOW, PHASE_LOW, PHASE_HIGH}
	};