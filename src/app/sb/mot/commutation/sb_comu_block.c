/*
 * sb_comu_block.c
 *
 * Created: 24.07.2019 22:16:21
 *  Author: vB1w
 */ 

#include "app/sb/mot/commutation/sb_comu_block.h"

void sb_comu_block_init(sb_comu_block *pself){
	
}

/* run this function cyclically after sensor input measurement over nvic*/
void sb_comu_block_run(sb_comu_block *pself){
	/* TODO:: get c code from scilab calculation for block commutation*/
	/* INPUT: Current measurement, rotor position, temperature*/
	/*	->
		calculation from scilab code conversion
	*/
	/* OUTPUT: duty cycle, average voltage on mosfets, failures*/
}