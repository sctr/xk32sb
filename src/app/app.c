/*
 * app.h
 *
 * Created: 19.01.2021 09:41:27
 *  Author: vB1w
 */ 

#include "app/app.h"

app s_app_sb = {
	.name = "Strawberry",
	.info = "Motor control system relying on xk32 plattform",
	.version = 0x00U
};

app s_app_ban = {
	.name = "Banana",
	.info = "Battery management system relying on xk32 plattform",
	.version = 0x00U
};

/*create task here*/
struct xk32_task_function s_xk32_task_fct[TASK_MAX];

struct xk32_task_task s_xk32_task_task = {
	.interval_ms = 3u,
	.is_interval_over = NO,
	.ctr = 0u,
	.task_list = s_xk32_task_fct
};

void app_init(struct app *pself){

/* system init (interface layer)*/
	/* --> includes:
	 * fill all struct from config file ?
	 * if_pm
	 * if_clock
	 * if_timer
	 * if_pio
	 * peripheral
	 * afec
	 */
	/*power management init*/
	if_pm_init(&s_if_pm);
	
	/*timer init*/
	if_timer_init(&s_if_timer);

	/*gpio init -> TODO:: pio in the first place, then as driver from gpio.h*/
	/* pio init*/
	pio_init(&s_pioa);
	pio_init(&s_pioc);
	pio_init(&s_piod);

	/* led init*/
	if_led_init();

	/* wdt init*/
	sb_wdt_init(SB_WDT_VALUE);
	/* afec init*/
	afec_init(&s_afec1);
	afec_init(&s_afec0);
	/* nvic_init -> TODO:: start general nvic from own .h modules, nvic.h stays,
	 * but is invoked by own .h modules as general nvic, internally there must
	 * be initialized an module enable irq bit*/
	nvic_init();
	/* init mot ctrl*/ //TODO:: uncomment for motor driver development
	//sb_mot_ctrl_init(&s_sb_mot_ctrl);
	
	/*TODO:: com/socket interface instead of term if*/
	/*term init*/
	if_term s_sb_term = {
		.enable = bTRUE,
		.is_init = bFALSE,
		
		.baudrate = SB_UART_BAUDRATE1M,
		.parity = ODD
	};
	if_term_init(&s_sb_term);
	
	xk32_term_out_msg("\n\r\n\r---uC initialized---\n\r\n\r");
	
	/*system check*/
	struct if_system_check s_sb_syschk = {
		.is_active = NO
		};
	if_sys_check_init(&s_sb_syschk);
}

void app_run(){

	xk32_task_register(&s_xk32_task_task, if_pm_run, &s_if_pm, "power_management");
	xk32_task_register(&s_xk32_task_task, if_sys_check_run, &s_sb_syschk, "system_check");
	xk32_task_register(&s_xk32_task_task, afec_run, &s_afec1, "analog_frontend_1");
	xk32_task_register(&s_xk32_task_task, afec_run, &s_afec0, "analog_frontend_0");
	xk32_task_register(&s_xk32_task_task, if_term_run, &s_sb_term, "uart_terminal");
	xk32_task_start(&s_xk32_task_task);

	while(1){

		xk32_task_update(&s_xk32_task_task);	
		// update led
		if_led_update(); // remove in later stage
		// update motor phase
		//sb_mot_ctrl_update(&s_sb_mot_ctrl);

	}
}