/*
 * ban.c
 *
 * Created: 09.09.2021 07:05:54
 *  Author: vB1w
 */ 

#include "drv/atsamd10c/defs.h"
#include "app/ban/ban.h"

ban s_ban = {
	.b_is_init = bFALSE,
	.pack = 0u,
	.pack_total = PACKAGE_TOTAL,
	.u8_err = 0x00u
	};

void ban_init(void){
	
}

void ban_run(void){
	
}
