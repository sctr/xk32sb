/*
 * sb.c
 *
 * Created: 20.03.2018 20:45:57
 * Author : vB1w
 */ 



#include "xk32/xk32_task.h"
#include "inc/app/app.h"

#define SB
/*#define BAN*/

#ifdef SB
#include "inc/app/sb/sb.h"
#endif

#ifdef BAN
#include "inc/app/ban/ban.h"
#endif

int main(void)
{
	
#ifdef SB
	app_init(&s_app_sb);
#endif

#ifdef BAN
	app_init(&s_ban_app);
#endif
	app_run();

}
